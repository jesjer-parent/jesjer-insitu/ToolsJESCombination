# ToolsJEScombination
This is the code for ToolsJEScombination, which is a toolkit for performing the statistical
combination of the JES and JER.  It is a simplified version of HVPTools.

# Authors   
For technical assistance, the primary point of contact is [Bogdan Malaescu](mailto:Bogdan.Malaescu@cern.ch) who is 
responsible for adapting the code to be used for the JES combination.

The full list of original authors are
  - Nicolas Berger (LAPP-Annecy)
  - Michel Davier (LAL-Orsay)
  - Andreas Hoecker (CERN)
  - Bogdan Malaescu (LAL-Orsay, now at LPNHE-Paris)
  - Zhiqing Zhang (LAL-Orsay)

Copyright: 2007, CERN, LAL-Orsay, LAPP-Annecy, LPNHE-Paris (All rights reserved)



# Usage

## Root Setup

### lxplus
Note that at the moment, this code can only be run on lxplus at CERN on account of the 
specific version of ROOT that is necessary - `5.34.24-x86_64-slc6-gcc48-opt``, which 
has always been setup via [the LCG distribution on AFS]().  

### Docker
This can also be used with Docker.  An image is build here [jesjer-parent/jesjer-insitu/root-builds](https://gitlab.cern.ch/jesjer-parent/jesjer-insitu/docker-root) 
with the docker image stored in the corresponding registry.  Assuming you have Docker setup
on your laptop, you can run this from the `ToolsJESCombination` directory (this repo) as
```
docker run --rm -it -v $PWD:$PWD gitlab-registry.cern.ch/jesjer-parent/jesjer-insitu/docker-root:master-c9e5fd51
```
which will load the image `gitlab-registry.cern.ch/jesjer-parent/jesjer-insitu/docker-root:master-c9e5fd51` and
mount (`-v` argument) the current `/path/to/ToolsJESCombination` within the image.  So when you are in the image
you can just move to that directory (it will be different for your system)
```
cd /path/to/ToolsJESCombination
```
and then perform the setup as below, except without the `setupATLAS` portion.

## Setup
Run setup and build the shared library:

```
cd ToolsJEScombination/
setupATLAS
lsetup "root 5.34.24-x86_64-slc6-gcc48-opt"	
source setup.sh # for bash or zsh. If you use [t]csh, please contact Nicolas or Andreas
mkdir lib
cd src
make
```
The library: ```ToolsJEScombination/lib/libToolsJEScombination.so``` should now exist, try:
```
ls -l ../lib/libToolsJEScombination.so
```

If this is successful, then it means you have built all of the underlying libraries.  Next you
will build the actual executable that steers the libraries.

## Run the example:
The example being used here is the combination for large-radius jets, hence the `LR`. 
It will take as input two different configuration files, meaning you will need to run
it twice, once for stats (`stats`) only and once for all systematics (`full`).

Start by building the executable itself
```
cd exe
make         # build the executable
```

Now run the combination which uses the stats-only configuration (`testappLR_stat.xml`).
The execution of this will/should be very fast.
```
./testappLR testappLR_stat.xml    
```

Now run the combination which uses the sull-systematics configuration (`testappLR_full.xml`).
The execution of this will take a bit longer, in the range of a few minutes.
```
./testappLR testappLR_full.xml    
```

In addition to the root file `testappLR.root` is created, which contains the 
data points (organised in directories for each channel), and a TTree with the 
integration results for each toy, some txt files with more details on the output 
of the combination are saved in `ToolsJEScombination/data/ComplexMergerResults_JES``.

## Plot the results of the merging:

Now plot the results 
```
cd ../data/ComplexMergerResults_JES/Results_2012
root -l -b PlotChannel.C+
```
Plot the chi^2 distribution of the merged measurements:
```
root -l -b chi2Distribution.C+
```
Plot the weights of the various inputs to the combination:
```
root -l -b PlotWeights.C+
```

# Organization - Directory structure:

Subdirectories of ToolsJEScombination/ package repository:
  - `src/`     : all source code (including class headers)
  - `include/` : contains class headers -> symbolic link to src 
  - `lib/`     : contains shared library `libToolsJEScombination.so`
  - `exe/`     : executables and driving data cards
  - `data/`    : the data XML in `./JES_root/JES_2012` as well as the output of the merging in `./ComplexMergerResults_JES/Results_2012`


# Programme structure:

The user interaction to ToolsJEScombination occurs via the driving and data XML cards, and by 
instantiating a "Controller" class object in the user executable (aka: `exe/testappLR.cxx`),
which organises all the work:
```
Controller ctr( "myDrivingCard.xml" );
ctr.Initialize();         // interpret driving and data cards
ctr.ExecuteAllActions();  // execute "actions" defined in driving card
ctr.Finalize();           // finalise the analysis
```

During "Initialisation", the data is read and placed in container class objects, 
which are hierarchically organised as follows:
```
    "GData" class object // globally accessible singleton class
       |
       +-- contains --> "Channel" class objects // stored in std::vector
                            |
                            +-- contains --> "MeasurementBase" class objects // std::vector
                                                   |
                                                   +-- contains --> "Data" class objects // std::vector
```

During "ExecuteAllActions", the actions defined in the "Actions" XML tag are 
executed in the given order ("T" or "F", respectively, switches actions on/off).

Subinformation for each action is given in aditional XML tags. For example, in "ComplexMerger" : 
  - `Ntoy` indicates the number of toys to be used at each step of the complex merger;
  - `AverageInterval` idicates the range in which the averaging is performed and "AverageFinalBinsN" the number of (fine) bins used for this averaging;
  - `SplineType` (1 or 2) indicates the type of splines used in the interpolation;
  - `EminExpMeas` : the extrapolation of the splines beyond the measurement limits is allowed only above this ("EminExpMeas") energy. No extrapolation is allowed in the provided example.
  - `DistanceRelExpMeas` : used to establish the range on which the extrapolation is performed, when allowed (see above)
  - `dMaxInterpolation` : sets an upper limit on the distance allowed between two points, in order for an interpolation to be performed. An error message is thrown if an interpolation over larger distances is requested.

The main part happens in the `ComplexMerger`. Here, the main method are `MergeChannel` and 
`ComputeComplexAverage`. The steps of the full toy analysis are as follows:

  - __(1)__ A first set of toys are generated, in order to derive the weight of each measurement in the fine bins. An intermediate rebinning (using large bins) is used in order to compare the statics of each measurement on the same footing.
  - __(2)__ The nominal result of the merging is derived: the nominal values for each point are used here.
  - __(3)__ New series of toys are generated for propagating the statistical and systematic uncertainties: the corresponding covariance matrices are derived.
  - __(4)__ A 1 sigma shift is applied for each nuisance parameter affecting the inputs, and the corresponding systematic uncertainty affecting the output of the metging is computed.
  - __(5)__ The results are computed and written to stdout, into a ROOT file and into several txt files.

# Class structure:
This is a list of ToolsJEScombination classes. `-->` indicates inheritance

## Data classes: 

  - GData, Channel, 
  - MeasurementBase, Data       (hierarchical data container classes)
  - MeasurementBase             (abstract base class for measurements)
  - `-->` UnitMeasurement         (holding a single measurement)

## Data interpolation and merging

  - TSplineBase                 (abstract base class for spline interpolations)
  - TSpline1                    (class implementing linear splines)
  - TSpline2                    (class implementing quadratic splines)
  - ComplexMergedMeasurement    (class implementing the merging procedure)

## XML reading and treatment

  - XMLInterpreter              (abstract base class for XML reading and parsing)
  - `-->` XMLDrivingInterpreter   (interprets driving CML card)
  - `-->` XMLDataInterpreter      (interprets data XML card, also creating the GData object)

## Data object and evaluator classes

  - Variable                    (a variable defined by a "normal" XML tag)
  - Formula                     (abstract formula)
  - `-->` SimpleConditionFormula  (extending TFormula by conditions, used by XML tag)
  - StringList                  (list of arguments obtained from XML string separated by ':')
  - `-->` Action                  (list of actions)
  - Interval                    (simple range definition)

## Utility classes:

  - GBase                       (global base class from which every class inherits)
  - GStore                      (holding all variables and actions)
  - Controller                  (global controller)
  - GConfig                     (global configuration variables)
  - Utils                       (namespace with all kinds of utility routines)
  - Timer                       (timing measurement - also draws the progress bar)
  - MsgLogger                   (all cout output is streamed through this logger)
  - RootClassFactory            (factory dynamically instantiating classes given by string name)
