// $Id: testapp.cxx,v 1.1 2007/07/16 23:23:22 hoecker Exp $    
// Author: Andreas Hoecker, Bogdan Malaescu

#include <iostream> // Stream declarations

#include "TFile.h"

#include "Controller.h"

using namespace std;

//---------------------------------------------------------------
// test example: read the xml file, merge measurements of a 
//               channel and be happy !
// ---------------------------------------------------------------

int main( int argc, char** argv ) 
{
   std::cout << "Start Test Application" << std::endl
             << "======================" << std::endl
             << std::endl;

   TString xmlFileName = "testappLR.xml";
   if (argc == 2) xmlFileName = argv[1];

   Controller ctr( xmlFileName );
   ctr.Initialize();
   ctr.ExecuteAllActions();
   ctr.Finalize();
}

