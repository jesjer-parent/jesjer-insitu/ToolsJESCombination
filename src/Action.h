// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Action_h
#define Action_h

#include <vector>

#include "StringList.h"

class TString;

class Action : public StringList {

 public:

   Action( const TString& name, const TString& arg );
   Action( const TString& name, std::vector<TString>& args ); 

   const TString& GetName()  const { return m_name; }
   Bool_t         IsActive() const { return m_active; }

 private:

   // remove first arg in list, which must be boolean
   void ExtractActiveFlag();

   const TString m_name;
   Bool_t        m_active;
};

#endif
