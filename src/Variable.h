// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Variable_h
#define Variable_h

#include "GBase.h"

class TString;
class StringList;

class Variable : public GBase {

 public:

   enum Type { Bool, Int, Double, String, ListOfStrings };

   Variable( const TString& name, const TString& value );
   ~Variable();

   // type of variable
   Type              GetType()         const { return m_type; }      
   TString           GetStrType()      const;
   const TString&    GetVariableName() const { return m_name; }      

   // retrieve value of variables
   Bool_t            GetBoolValue()   const;
   Int_t             GetIntValue()    const;
   Double_t          GetDoubleValue()  const;
   const TString&    GetStringValue() const;
   const StringList& GetStringList()  const;

   const TString&    GetValue() const { return m_value; }

   void Print( std::ostream& os ) const;
    
 private:

   Type          m_type;
   const TString m_name; 
   const TString m_value; 

   Bool_t        m_b; 
   Int_t         m_i; 
   Double_t       m_f; 
   TString       m_s;
   StringList*   m_sl;
};

#endif
