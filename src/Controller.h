// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Controller_h
#define Controller_h

#include <map>

#include "GBase.h"

class TFile;
class TXMLNode;
class XMLInterpreter;

class Controller : public GBase {
      
 public:
      
   Controller( const TString& drivingDataCard );
   virtual ~Controller();

   // called after interpretation of datacard
   void Initialize();

   // called in between initialize and terminate
   void ExecuteAllActions();

   // must be called at end of job
   void Finalize();

   // retrieve output file
   TFile& GetTargetFile() const { return *m_targetFile; }
      
 private:

   // interpretation of data card
   std::map<const TString, XMLInterpreter*> m_xmlInterpreter;

   // target output file
   TFile* m_targetFile;

   ClassDef(Controller,0);

};

#endif
