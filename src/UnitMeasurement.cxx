// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <iostream>

#include "TGraph.h"
#include "TVectorD.h"
#include "TGraphErrors.h"
#include "TFormula.h"

#include "UnitMeasurement.h"
#include "Data.h"
#include "Interval.h"
#include "SimpleConditionFormula.h"
#include "TSplineBase.h"
#include "TSplineFactory.h"
#include "GStore.h"

using std::cout;
using std::endl;

UnitMeasurement::UnitMeasurement( const TString& channelName, 
                                  const TString& alias,
                                  const TString& experiment,
                                  const TString& reference, 
                                  const TString& comment, 
                                  const TString& systError, 
                                  Bool_t active )
   : MeasurementBase( channelName, alias, experiment ),
     m_reference  ( reference ), 
     m_comment    ( comment ),   
     m_systError  ( systError ), 
     m_active     ( active )
     //     m_systErrorF ( 0 )
{
   SetName( GetType().Data() );

   Init();
}

void UnitMeasurement::Init()
{
   // base class init (for initialisation flags)
   GBase::Init();

   // parse formula for systematic error
   TString systStr = GetSystError();
   if( systStr == "" ){
      m_logger << kINFO << " Systematic error string for " << GetExperiment() << " is empty at this level (components should be added later). " << Endl;
      return;
   }
   m_logger << kINFO << " Systematic error for " << GetExperiment() << " : " << systStr << Endl;
   systStr.ToLower();
   if ( !systStr.Contains( "=" ) && !systStr.Contains( "individual" ) && !systStr.Contains( "none" ) ) 
     {
       // we have one formula for the "global" systematic error

       // if no detail is given, treat different experiments as uncorrelated (for the beginning)
       AddSystErrName( new TString( "global" + GetExperiment() ) );
       m_systErrNamesForm.push_back( new TString( "global" + GetExperiment()) );
       m_systErrorFVec.push_back(new SimpleConditionFormula( systStr ));
     }  
   else 
     if ( systStr.Contains( "=" ) )
       {
         // we should have the details of the systematic errors

         // potential problems for formulas starting by "-"
         systStr.ReplaceAll( "+-", "@" );
         // "+-" should be put between two components of the systematic error
         TObjArray* toklist1 = systStr.Tokenize( "@" );

         if ( toklist1->GetEntries() > 0 ) 
           {
             // iterate over the components of the systematic errors
             for(Int_t i=0;i<toklist1->GetEntries();i++)
               {
                 // save the name (and the formula, if the error is given in such a format) of one component of the systematic error
                 TObjArray* toklist2 =((TObjString*)toklist1->At(i))->GetString().Tokenize("=");
                 
                 if ( toklist2->GetEntries() == 2 )
                   {
                     TString tempStr0 = ((TObjString*)toklist2->At(0))->GetString();
                     TString tempStr1 = ((TObjString*)toklist2->At(1))->GetString();

                     if ( tempStr1.Contains( "individual" ) )
                       {
                         m_systErrNamesInd.push_back( new TString(tempStr0) );
                       }
                     else
                       {
                         m_systErrNamesForm.push_back( new TString(tempStr0) );
                         m_systErrorFVec.push_back(new SimpleConditionFormula( tempStr1 ));
                       }
                   }
                 else
                   {
                     // each component of the systematic error should have the format "name = formula" or "name = individual"
                     m_logger << kFATAL << "Error in the format of systematic errors: toklist2 length: " 
                              << toklist2->GetEntries() << " whereas 2 parameters were expected" << Endl;
                   }
           
                 delete toklist2;

               }

             // now fill the vector of the names of all the systematic errors: individualy given errors first, then the names of the systematics given by formulas!!!
             std::vector<TString*>::iterator it;
             for( it = m_systErrNamesInd.begin(); it != m_systErrNamesInd.end(); it++ )
               AddSystErrName( new TString( **it ) );

             for( it = m_systErrNamesForm.begin(); it != m_systErrNamesForm.end(); it++ )
               AddSystErrName( new TString( **it ) );

           }

         else{
           m_logger << kFATAL << "Error in the format of systematic errors: toklist1 length: " 
                    << toklist1->GetEntries() << "  whereas several systematic errors were expected " << Endl;
         }
       
         delete toklist1;
       }
     else
       {
         if ( systStr.Contains( "individual" ) )
           {
             // if no detail is given, treat different experiments as uncorrelated (for the beginning)
             AddSystErrName( new TString( "global" + GetExperiment() ) );
             m_systErrNamesInd.push_back( new TString( "global" + GetExperiment() ) );
           }
       }

   //sanity check (each error name should occure only once in the vector with all the names)
   if(  GetSystErrNamesAll().size()>1 )
     {
       std::vector<TString*>::iterator it1 =  GetSystErrNamesAll().begin(), it2;
       it1++;
       for( ; it1!= GetSystErrNamesAll().end(); it1++ )
         for( it2 =   GetSystErrNamesAll().begin(); it2<it1; it2++ )
           if( (**it1) == (**it2) )
             m_logger << kFATAL << "Error in the format of systematic errors: double occurence of " 
                      << **it1 << " in " << m_reference <<Endl;
     } 

}

UnitMeasurement::~UnitMeasurement()
{
   if ( m_systErrNamesForm.size() ) m_systErrNamesForm.clear();
   if ( m_systErrorFVec.size() ) m_systErrorFVec.clear();
   if ( m_systErrNamesInd.size() ) m_systErrNamesInd.clear();
}


void UnitMeasurement::AddData( Data* data )
{
   if (data == 0) m_logger << kFATAL << "<AddData> Zero pointer" << Endl;

   // sanity check for energy overlaps
   if (GetDataVec().size() > 0) {
      if (data->Emin() < GetDataVec().back()->Emax()) {
         m_logger << kWARNING << "<AddData> Overlap problem in " << GetDataVec().size()+1 << "th data point "
                  << "of " << GetChannelName() << " : " << GetExperiment() << " : "
                  << GetDataVec().back()->Emax() << " > " << data->Emin() << Endl;
         data->Print( std::cout );
      }
   }

   // interpret systematic error expression (relative error given in %)
   // only if non-individual systematic errors are given
   if ( m_systErrorFVec.size()>0 ) {

     std::vector<Formula*>::iterator it;
     std::vector<Double_t> globalSyst;

     for(it = m_systErrorFVec.begin(); it!=m_systErrorFVec.end(); it++){

      std::vector<Double_t> pars;
      pars.push_back( data->Eave() );
      pars.push_back( data->Xsec() );
      
      Double_t e = (*it)->Eval( pars )*data->Xsec();

      data->AppendeXsecSystVec( e );

     }
     
   }

   // at last, update experiment ID
   data->SetExpId( GetExpId() );

   // unable to use splines if the distance between the data in the measurement is too big
   if( GetDataVec().size() > 0 ){
     if( data->GetEDistance( *GetDataVec().back() ) > Get_dMax() )
        {
           m_logger << kFATAL << "Splines cannot be used for the experiment " << GetExperiment() << ", although needed for the JES combination !!! Too big data distance: " << data->GetEDistance( *GetDataVec().back() ) << Endl; 
           SetUseSplines( kFALSE );
        }
   }

   // test if data is a point
   Double_t eps = 1e-7;
   if( GetOnlyBins() && (data->deltaE()<eps) ){
     SetOnlyBins( kFALSE );
   }

   // append to vector
   GetDataVec().push_back( data );   
}

Bool_t UnitMeasurement::HasContribution( const Interval* I)
{
  if( IsActive() )
    {
      if( GetUseSplines() )
        {// if we use splines we have to compare the positions of two intervals (of the measurement and of the bin)
          Interval::EStatus status = ( GetCEnergyInterval() ).GetEStatus( *I );
          
          if( status==Interval::kSmaller || status==Interval::kLarger ){ 
            return kFALSE;
          }
          else{
            return kTRUE;
          }
        }
      else
        { // should never reach this point
	  m_logger << kFATAL << "Splines must be used for the JES combination !!!" << Endl;
        }
    }

  // we don't use splines and we found no contributing initial bin
  return kFALSE;
}

void UnitMeasurement::GenerateMeasurementValues( UInt_t askedSplineType, TRandom3& R, const TArrayD& coeff_m, const std::vector<Bool_t>& contributionSmall, const std::vector<Interval*>& binningSmall, const std::vector<Bool_t>& contributionLarge, const std::vector<Interval*>& binningLarge, TMatrixD* matrix, UInt_t lineNumber, TVectorD* &toyXSec, UInt_t _activStat, UInt_t _useIntermLargeBins, UInt_t _activSyst )
{
  // sanity check
  if( _useIntermLargeBins && ( (!_activStat) || (!_activSyst) ) )
    m_logger << kFATAL << " Intermediate large bins can be used only with complete fluctuations (to get the average coefficients) !!!" << Endl;

  // generate the toy data values and save the parameters for the TSplineBase
  toyXSec = new TVectorD( GetDataVec().size() );
  TVectorD *binCenters = new TVectorD( GetDataVec().size() );
  std::vector<Double_t> binsEnergySize;
  Int_t _NfilledBinsLarge = 0; // number of large bins filled by this measurement
  for( UInt_t i=0; i < binningLarge.size(); i++ ){
     if( contributionLarge.at(i) ){ _NfilledBinsLarge++; }
  } 

  for( UInt_t it=0; it<GetDataVec().size(); it++ )
    {
      (*toyXSec)[it] = GetDataVec().at(it)->GenerateValue( R, coeff_m, _activStat, _activSyst );
      (*binCenters)[it] = GetDataVec().at(it)->Eave();
      if( GetOnlyBins() ){
        binsEnergySize.push_back( GetDataVec().at(it)->deltaE() );
      }
    }

  // evaluate the measurement on each final bin
  if ( GetUseSplines() )
    {
      // create a spline with the toy measurement
      TGraph* toyGraph = new TGraph( *binCenters, *toyXSec );
      TSplineBase *toySpline = 0, *toySplineUse = 0;
      if( askedSplineType == 1 ){
         toySpline = TSplineFactory::CreateTSpline( "Toy", toyGraph, "TSpline1", GetOnlyBins(), binsEnergySize );
      }
      else{
        if( askedSplineType == 2 ){
           toySpline = TSplineFactory::CreateTSpline( "Toy", toyGraph, "TSpline2", GetOnlyBins(), binsEnergySize );
        }
        else{
          m_logger << kFATAL << "Not implementes spline type: " << askedSplineType << Endl;
        }
      }

      Double_t valLarge1bin = 0.; // Xsec value to be used if the experiment fills only one large bin (splines can not be used)
      if( _useIntermLargeBins && (_NfilledBinsLarge>0) ){ 
        TVectorD *binCentersLarge = new TVectorD( _NfilledBinsLarge );
        std::vector<Double_t> binsEnergySizeLarge;
        UInt_t j = 0;
        for( UInt_t i=0; i < binningLarge.size(); i++ ){
           if( contributionLarge.at(i) ){ 
            (* binCentersLarge)[j] = binningLarge.at(i)->GetMean(); 
            binsEnergySizeLarge.push_back( binningLarge.at(i)->GetWidth() );
            j++;
          }
        } 

        TVectorD *toyXSecLarge = new TVectorD( _NfilledBinsLarge );
        j = 0;
        for( UInt_t i=0; i < binningLarge.size(); i++ ){
           if( contributionLarge.at(i) ){ 
            Double_t frac = 0., leftL = 0., rightL = 0.;
            frac = ( GetCEnergyInterval() ).GetFracInterv( binningLarge.at(i)->GetMin(), binningLarge.at(i)->GetMax(), leftL, rightL );
            if( frac > 1e-7 ){
              (*toyXSecLarge)[j] = ( toySpline->EvalIntegral( leftL, rightL ) ) / ( rightL - leftL ); 
	    }
            j++;
          }
        } 
        if(_NfilledBinsLarge == 1){
           valLarge1bin = (*toyXSecLarge)[0];
        }
        else{
          TGraph* toyGraphLarge = new TGraph( *binCentersLarge, *toyXSecLarge );
          if( askedSplineType == 1 ){
             toySplineUse = TSplineFactory::CreateTSpline( "ToyLarge", toyGraphLarge, "TSpline1", kTRUE, binsEnergySizeLarge );
          }
          else{
            if( askedSplineType == 2 ){
               toySplineUse = TSplineFactory::CreateTSpline( "ToyLarge", toyGraphLarge, "TSpline2", kTRUE, binsEnergySizeLarge );
            }
            else{
              m_logger << kFATAL << "Not implementes spline type: " << askedSplineType << Endl;
            }
          }
          delete toyGraphLarge;
        }
        delete toyXSecLarge;
        delete binCentersLarge;
        binsEnergySizeLarge.clear();
      }
      else{
        toySplineUse = toySpline;
      }

      // evaluate the value on each final bin, if contributionSmall, and fill the matrix
      for( UInt_t i=0; i<binningSmall.size(); i++ )
        {
           if( contributionSmall.at(i) )
            {
              if( _useIntermLargeBins && (_NfilledBinsLarge == 1) ){
                (*matrix)( lineNumber, i ) = valLarge1bin;
              }
              else{
                (*matrix)( lineNumber, i ) = ( toySplineUse->EvalIntegral( binningSmall.at(i)->GetMin(), binningSmall.at(i)->GetMax() ) ) / binningSmall.at(i)->GetWidth();
              }
            }
        }

      if( _useIntermLargeBins && (_NfilledBinsLarge>0) ){ 
        delete toySplineUse;
      }
      delete toySpline;
      delete toyGraph;
    }
  else
    {
      // should never reach this point
      m_logger << kFATAL << "Splines must be used for the JES combination !!!" << Endl;
    }

  delete binCenters;
  binsEnergySize.clear();
}

void UnitMeasurement::Print( std::ostream& os ) 
{   
   os << Form( "UnitMeasurement: Exp = \"%s\" (\"%s\"), Ref = \"%s\", Comment = \"%s\" (active = %c) [ID: %i]",
               GetExperiment().Data(), GetAlias().Data(), GetReference().Data(), GetComment().Data(), 
               (IsActive() ? 'T' : 'F'), GetExpId() )
      << std::endl;
   os << std::endl;

   os << "Syst errors names: " ;
   for(UInt_t i=0; i<GetSystErrNamesAll().size(); i++){
      os << *(GetSystErrNamesAll().at(i)) << "  ";
   }
   os << std::endl;
   
   // iterate over all data and print them
   std::vector<Data*>::iterator it;
   for (it = GetDataVec().begin(); it != GetDataVec().end(); it++) {
      os << "      "; (*it)->Print( os );
   }
   os << std::endl;
}

void UnitMeasurement::PrintMeasurementStatus()
{
  //all the data should be added before to call this function
  if( GetOnlyBins() ){
    m_logger << kINFO << "The measurement " << GetExperiment() << " has " << GetDataVec().size() 
             << " data bins and ";
  }
  else {
    m_logger << kINFO << "The measurement " << GetExperiment() << " has " << GetDataVec().size() 
             << " data points and ";
  }

  if(GetDataVec().size()>2){
    m_logger << kINFO << "TSpline1 and TSpline2 can be used." ;
  }
  else{
    if( GetDataVec().size()==2 ){
      m_logger << kINFO << " only TSpline1 can be used." ;
    }
    else{
      m_logger << kINFO << " no splines can be used." ;
    }
  }

  if( GetOnlyBins() ){
    m_logger << kINFO << " It has only bins. If requested, integrals will be conserved..." << Endl;
  }
  else{
    m_logger << kINFO << " It doesn't have only bins. No integral to conserve..." << Endl;
  }
}
