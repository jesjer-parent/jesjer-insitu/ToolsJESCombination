// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include <iostream>

#include "Interval.h"

const char* Interval::EStatusStr[] = { "Smaller", "O-Smaller", "Embracing", 
                                   "Identical", "Within", "O-Larger", "Larger" };  

Interval::Interval( Double_t min, Double_t max )
   : GBase( "Interval" ),
     m_min( min ), 
     m_max( max )
{
   // sanity check
  if (m_min > m_max) m_logger << kFATAL << "min > max:0 " << m_min << " , " << m_max << Endl;
}

Interval::Interval( const Interval& interval )
   : m_min( interval.GetMin() ), 
     m_max( interval.GetMax() )
{
   SetName( "Interval" );

   // sanity check
   if (m_min > m_max) m_logger << kFATAL << "min > max:1 " << m_min << " , " << m_max << Endl;
}

Interval::Interval( const Interval* interval )
   : m_min( interval->GetMin() ), 
     m_max( interval->GetMax() )
{
   SetName( "Interval" );

   // sanity check
   if (m_min > m_max) m_logger << kFATAL << "min > max:2 " << m_min << " , " << m_max << Endl;
}

Interval::~Interval()
{}

void Interval::SetMinMax( Double_t min, Double_t max ) 
{ 
  m_min = min; 
  m_max = max;

  // sanity check
  if (m_min > m_max) m_logger << kFATAL << "min > max:3 " << m_min << " , " << m_max << Endl;
}

std::ostream& operator << ( std::ostream& os, const Interval& interval ) 
{
   os << "[" << interval.GetMin() << ", " << interval.GetMax() << "]";
   return os;
}

Interval::EStatus Interval::GetEStatus( const Interval& I ) const 
{ 
   Interval::EStatus es = kSmaller; 
   if      (I.GetMax() == GetMax() && I. GetMin() == GetMin()) es = kIdentical;
   else if (I.GetMax() <= GetMin()                           ) es = kSmaller;
   else if (I.GetMin() >= GetMax()                           ) es = kLarger;
   else if (I.GetMax() >= GetMax() && I.GetMin() <= GetMin() ) es = kEmbracing;
   else if (I.GetMax() <= GetMax() && I.GetMin() >= GetMin() ) es = kWithin;
   else if (I.GetMax() >= GetMax() && I.GetMin() >= GetMin() ) es = kOverlappingLarger;
   else if (I.GetMax() >= GetMin() && I.GetMin() <= GetMin() ) es = kOverlappingSmaller;
   else {
      m_logger << kFATAL << "<GetEStatus> Impossible energy interval status: my data: " 
               << GetMin() << " " << GetMax() << ", ref data: "
               << I.GetMin() << " " << I.GetMax() 
               << Endl;
   }

   return es;
}

// find the fraction of the (*this)interval inside [EMin;EMax], and the common interval
Double_t Interval::GetFracInterv( const Double_t EMin, const Double_t EMax, Double_t &left, Double_t &right ) const 
{ 
  Double_t frac = 1.;
  left = GetMin();
  right = GetMax();

  if      (EMax == GetMax() && EMin == GetMin() ){ frac = 1.; }
  else if (EMax <= GetMin()                     ){ frac = 0.; left = 0.; right = 0.; }
  else if (EMin >= GetMax()                     ){ frac = 0.; left = 0.; right = 0.; }
  else if (EMax >= GetMax() && EMin <= GetMin() ){ frac = 1.; } 
  else if (EMax <= GetMax() && EMin >= GetMin() ){ frac = (EMax-EMin)/(GetMax()-GetMin()); 
                                                   left = EMin; 
                                                   right = EMax;
                                                 }
  else if (EMax >= GetMax() && EMin >= GetMin() ){ frac = (GetMax()-EMin)/(GetMax()-GetMin()); 
                                                   left = EMin;
                                                   right = GetMax();
                                                 }
  else if (EMax >= GetMin() && EMin <= GetMin() ){ frac = (EMax-GetMin())/(GetMax()-GetMin()); 
                                                   left = GetMin();
                                                   right = EMax;
                                                 } 
   else {
      m_logger << kFATAL << "<GetEStatus> Impossible energy interval status: my data: " 
               << GetMin() << " " << GetMax() << ", ref data: "
               << EMin << " " << EMax 
               << Endl;
   }

   return frac;
}

Double_t Interval::GetFracInterv( const Double_t EMin, const Double_t EMax ) const 
{
  Double_t l, r;
  return GetFracInterv( EMin, EMax, l, r );
}
