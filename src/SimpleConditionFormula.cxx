// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TObjString.h"
#include "TString.h"

#include "SimpleConditionFormula.h"
#include "Utils.h"

SimpleConditionFormula::SimpleConditionFormula( const TString& expression )
   : Formula( expression )
{
   m_logger.SetMinType( kINFO );
   SetName( "SimpleConditionFormula" );
   ParseExpression();   
}

SimpleConditionFormula::~SimpleConditionFormula()
{}

Double_t SimpleConditionFormula::Eval( std::vector<Double_t>& pars ) const
{
   // first parameter in vector is energy, second is Xsec
   if (pars.size() != 2) m_logger << kFATAL << "Wrong vector size: " << pars.size() << Endl;

   for (UInt_t i=0; m_condVec.size(); i++) {
      if (pars[0] > m_condVec[i]) {
         for (UInt_t j=0; j<pars.size(); j++) m_exprVec[i]->SetParameter( j, pars[j] );
         return m_exprVec[i]->Eval( 0 );
      }
   }
   
   m_logger << kFATAL << "<ParseExpression> Should never have arrived here..." << Endl;

   return -1;      
}

void SimpleConditionFormula::ParseExpression()
{
   m_logger << kVERBOSE << "Interpreting formula expression: " << GetExpression() << Endl;

   // sanity check
   if (GetExpression().Contains( "<" ) || 
       GetExpression().Contains( "=" ) ||
       GetExpression().Contains( "MeV" )) {
      m_logger << kFATAL << "Error in condition expression" << Endl;
   }

   m_condVec.clear();
   m_exprVec.clear();
   
   TString condition = GetExpression();

   // remove units
   condition.ReplaceAll( "gev", "" );
   condition.ReplaceAll( "%", "*0.01" );

   // cut into independent conditions
   TList* arglist = Utils::ParseFormatLine( condition, ":" );

   // loop over conditions
   for (Int_t iarg=0; iarg<arglist->GetSize(); iarg++) {

      TString arg = ((TObjString*)arglist->At(iarg))->GetString();

      // continue parsing subelements
      TList* elmlist = Utils::ParseFormatLine( arg, "?" );

      TString cond, expr;
      if (elmlist->GetSize() == 2) {
         TList* cndlist = Utils::ParseFormatLine( ((TObjString*)elmlist->At(0))->GetString(), ">" );
         if (cndlist->GetSize() != 2) {
            m_logger << kFATAL << "Error: cond list should have two entries: " << cndlist->GetSize() << Endl;
         }
         cond = ((TObjString*)cndlist->At(1))->GetString();
         expr = ((TObjString*)elmlist->At(1))->GetString();

         delete cndlist;
      }
      else {
         cond = "-1";
         expr = ((TObjString*)elmlist->At(0))->GetString();
      }

      delete elmlist;

      expr.ToLower();

      // replace "xsec" and "e" by parameters
      if (expr.Contains("xsec")) expr.ReplaceAll( "xsec", "[1]" );
      if (expr.Contains("e"))    expr.ReplaceAll( "e",    "[0]" );

      // create formula for expression, and check for sanity
      TFormula* exprF = new TFormula( Form( "f%i", (Int_t)m_exprVec.size() ), expr );
      if (exprF->GetNdim() != 1) {         
         m_logger << kFATAL << "Error, TFormula could not parse expression: " << expr << Endl;
      }
      if (exprF->GetNpar() > 3) {         
         m_logger << kFATAL << "Error, TFormula has wrong number of parameters: " << expr << Endl;
      }

      // fill vectors
      TFormula* condF = new TFormula( "cond", cond ); // useful if energy limits are given by a formula
      m_condVec.push_back( condF->Eval( 0. ) );
      delete condF;
      m_exprVec.push_back( exprF );
   }     

   delete arglist;

   // sanity check
   for (UInt_t i=1; i<m_condVec.size(); i++) {
      if (m_condVec[i] >= m_condVec[i-1]) {
         m_logger << kFATAL << "Error in condition hierarchy: " << m_condVec[i-1] << " " << m_condVec[i] << Endl;
      }
   }

   // print out
   for (UInt_t i=0; i<m_condVec.size(); i++) {
      m_logger << kVERBOSE << "--- " << i << "th condition : " << m_condVec[i] << Endl;
      m_logger << kVERBOSE << "--- " << i << "th expression: " << m_exprVec[i]->GetExpFormula() << Endl;
   }
}
