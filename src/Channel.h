// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Channel_h
#define Channel_h

#include <vector>
#include "GBase.h"

class TDirectory;
class TFile;
class MeasurementBase;

class Channel : public GBase {

 public:
      
   Channel( const TString& name );
   virtual ~Channel();
      
   void AddMeasurement ( MeasurementBase* );

   // accessors
   const TString& GetName()       const { return m_name; }
   const TString& GetRegexpName() const { return m_regexpName; }

   // access to measurement vector
   std::vector<MeasurementBase*>& GetMeasurementVec()       { return m_measurements; }
   std::vector<MeasurementBase*>& GetMergedMeasurementVec() { return m_Mmeasurements; }

  // acces to the names of ALL systematic errors occuring in one channel
  void GetAllSystErrNamesChannel( std::vector<TString*>& allSystErrNamesChannel );  
  // get the matrix indicating which are the measurements that have correlated errors
  void GetCorrMeasurementsChannel( std::vector< std::vector<Bool_t> >& corrMeasurementsChannel );  

   // compute the merged channel 
   MeasurementBase* MergeMeasurements( Bool_t addToList = kFALSE, const TString& mergerName = "" );
   
   // create plot with all measurements of this channel
   void CreateGraphs( TFile& );

   // local directory in target file
   TDirectory& GetLocalDir( TFile* file = 0 );

   // is channel active ?
   Bool_t IsActive() const { return m_active; }

   // print the class content
   void Print( std::ostream& );

 private:

   // meta data
   TString m_name;   
   TString m_regexpName;
   Bool_t  m_active;

   // the measurements
   std::vector<MeasurementBase*> m_measurements;

   // the vector of merged  measurements introduced in the order of the arguments of the "MergeMeasurements" action, if any
   std::vector<MeasurementBase*> m_Mmeasurements;

   // local directory for plots
   TDirectory* m_localDir;

};

#endif
