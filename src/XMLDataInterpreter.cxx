// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TObjArray.h"
#include "TObjString.h"
#include "TDOMParser.h"
#include "TXMLNode.h"
#include "TXMLDocument.h"
#include "TXMLAttr.h"
#include "TString.h"
#include "GStore.h"

#include "XMLDataInterpreter.h"
#include "GData.h"
#include "Channel.h"
#include "UnitMeasurement.h"
#include "Data.h"
#include "Utils.h"

XMLDataInterpreter::XMLDataInterpreter( const TString& xmlFileName )
   : XMLInterpreter( xmlFileName )
{
   SetName( "XMLDataInterpreter" );
}

XMLDataInterpreter::~XMLDataInterpreter()
{}

Bool_t XMLDataInterpreter::Interpret() const
{
   // --------------- xml read
   m_logger << kINFO << "Read xml data card: \"" << GetXMLFileName() << "\"" << Endl;
  
   TDOMParser* xmlparser = new TDOMParser();
      
   Int_t parseCode = xmlparser->ParseFile( GetXMLFileName() );

   m_logger << kINFO << "XML parser returned code: " << parseCode << Endl;
   if (parseCode != 0) m_logger << kFATAL << "loading of xml document failed" << Endl;
  
   // --------------- parse GlobaData Root

   TXMLDocument* xmldoc = xmlparser->GetXMLDocument();

   TXMLNode* globalData_node = xmldoc->GetRootNode();
   TXMLNode* globalData_elem = globalData_node->GetChildren();
  
   Int_t nchan = 0;
   while (globalData_elem != 0) {
      if      (globalData_elem->GetNodeName() == TString("Channel"))      { nchan++; this->ReadMeasurements( globalData_elem ); }

      // crawl on...
      globalData_elem = globalData_elem->GetNextNode();
   }

   m_logger << kINFO << "Interpretation of data card done! (Total of " << nchan << " channels found)" << Endl;

   return kTRUE;
}

void XMLDataInterpreter::ReadMeasurements( TXMLNode* channelNode ) const
{
   // sanity check for the dataNode
   if (!channelNode->HasChildren()) {
      m_logger << kFATAL << "<Channel> ... </Channel> Part does not contain any parameters" << Endl;
   }

   // attribute list for channel node
   const TList* channelAttribs = channelNode->GetAttributes();

   // channel name
   TXMLAttr* curAttr = (TXMLAttr*)channelAttribs->FindObject( "Name" );
   if (curAttr == 0) m_logger << kFATAL << "No \"Name\" attribute given" << Endl;
   TString chName  = curAttr->GetValue();
   chName.ReplaceAll( " ", "" );

   // create a new channel --> however, do not add it if not active
   Channel* channel = new Channel( chName );
   if (!channel->IsActive()) {
      delete channel;
      return;
   }

   // retrieve children of channel node (-> measurement)
   TXMLNode* measurementNode = channelNode->GetChildren();
   for (; measurementNode != 0; measurementNode = measurementNode->GetNextNode()) {
      if (TString("Measurement") != measurementNode->GetNodeName()) continue;

      // sanity checks for the measurement      
      if (!measurementNode->HasAttributes()) {
         m_logger << kFATAL << "<Measurement> ... </Measurement> Part does not contain any attributes" << Endl;
      }
      if (!measurementNode->HasChildren()) {
         m_logger << kFATAL << "<Measurement> ... </Measurement> Part does not contain any children" << Endl;
      }
      
      // attribute list for measurement node
      const TList* measurementAttribs = measurementNode->GetAttributes();
      
      // Measurement attributes
      curAttr = (TXMLAttr*)measurementAttribs->FindObject( "Channel" );
      if (curAttr == 0) m_logger << kFATAL << "No \"Channel\" attribute given" << Endl;
      TString chan  = curAttr->GetValue(); 
      chan.ReplaceAll( " ", "" );
      // sanity check
      if (chan != chName) {
         m_logger << kFATAL << "Mismatch in channel name: " << chan << " != " << chName << Endl;
      }
      Utils::CleanString( chan );

      curAttr = (TXMLAttr*)measurementAttribs->FindObject( "Experiment" );
      if (curAttr == 0) m_logger << kFATAL << "No \"Experiment\" attribute given" << Endl;
      TString experiment  = curAttr->GetValue(); Utils::CleanString( experiment );

      TString alias  = "";
      curAttr = (TXMLAttr*)measurementAttribs->FindObject( "Alias" );
      if (curAttr != 0) alias  = curAttr->GetValue(); Utils::CleanString( alias );

      curAttr = (TXMLAttr*)measurementAttribs->FindObject( "Reference" );
      if (curAttr == 0) m_logger << kFATAL << "No \"Reference\" attribute given" << Endl;
      TString reference  = curAttr->GetValue();

      curAttr = (TXMLAttr*)measurementAttribs->FindObject( "Comment" );
      if (curAttr == 0) m_logger << kFATAL << "No \"Comments\" attribute given" << Endl;
      TString comment  = curAttr->GetValue();

      curAttr = (TXMLAttr*)measurementAttribs->FindObject( "Active" );
      if (curAttr == 0) m_logger << kFATAL << "No \"Active\" attribute given" << Endl;
      TString active  = curAttr->GetValue(); Utils::CleanString( active );
      // sanity check
      if (active != "T" && active != "F") {
         m_logger << kFATAL << "Problem in \"Active\" attribute of channel: " << chName 
                  << " --> " << active << Endl;
      }

      if( active == "T" ){
         curAttr = (TXMLAttr*)measurementAttribs->FindObject( "SystError" );
         if (curAttr == 0) m_logger << kFATAL << "No \"SystError\" attribute given" << Endl;
         TString systError = curAttr->GetValue(); 

         Utils::CleanString( systError );
         
          TString *systError_tmp = new TString(systError);
          systError_tmp->ReplaceAll("_AsymSyst","");
         // create a new Measurement      
         MeasurementBase* measurement = new UnitMeasurement( chName, alias, experiment, reference, comment, (*systError_tmp), 
                                                             (active == "T" ? kTRUE : kFALSE) );
         delete systError_tmp;

            // retrieve children of measurementNode
            TXMLNode* dataNode = measurementNode->GetChildren();
            for (; dataNode != 0; dataNode = dataNode->GetNextNode()) {      
               if (TString("Data") != dataNode->GetNodeName()) continue;
               
               // sanity check for the data node
               if (!dataNode->HasAttributes()) {
                  m_logger << kFATAL << "<Data> ... </Data> Part does not contain any attributes" << Endl;
               }
               
               // attribute list for data node
               const TList* dataAttribs = dataNode->GetAttributes();
               
               // Data parameters
               curAttr = (TXMLAttr*)dataAttribs->FindObject( "Energy" );
               if (curAttr == 0) m_logger << kFATAL << "No \"Energy\" attribute given" << Endl;
               TString energyStr  = curAttr->GetValue();
               Int_t energyStrIs_s = 0;
               if( energyStr.Contains( "GeV2" ) || energyStr.Contains( "GeV^2" ) ){
                  energyStrIs_s = 1;
                  energyStr.ReplaceAll( "GeV2", "" );
                  energyStr.ReplaceAll( "GeV^2", "" );
               }

               // parse the energy string
               Double_t Emin = -1, Emax = -1;

               TObjArray* toklist = energyStr.Tokenize( ":" );
               if( energyStrIs_s ){
                  if (toklist->GetEntries() == 1) {
                     Emin = Emax = sqrt( ((TObjString*)toklist->At(0))->GetString().Atof() );
                  }
                  else if (toklist->GetEntries() == 2) {
                     Emin = sqrt( ((TObjString*)toklist->At(0))->GetString().Atof() );
                     Emax = sqrt( ((TObjString*)toklist->At(1))->GetString().Atof() );
                  }
                  else {
                     m_logger << kFATAL << "Problems in Energy attribut of Data (s): " << toklist->GetEntries()
                              << Endl;
                  }
               }
               else{
                  if (toklist->GetEntries() == 1) {
                     Emin = Emax = ((TObjString*)toklist->At(0))->GetString().Atof();
                  }
                  else if (toklist->GetEntries() == 2) {
                     Emin = ((TObjString*)toklist->At(0))->GetString().Atof();
                     Emax = ((TObjString*)toklist->At(1))->GetString().Atof();
                  }
                  else {
                     m_logger << kFATAL << "Problems in Energy attribut of Data: " << toklist->GetEntries()
                              << Endl;
                  }
               }
               // If requested, replace bins by points
               gStore().AssertVariable( "ComplexMerger::ReplaceBinsByPoints" );
               Bool_t replaceBinsByPoints = gStore().GetVariable( "ComplexMerger::ReplaceBinsByPoints" )->GetBoolValue();
               if( replaceBinsByPoints ){ 
                  Double_t pointEn = (Emin + Emax)/2.; 
                  Emin = Emax = pointEn; 
               } 
               
               // parse the cross section string
               curAttr = (TXMLAttr*)dataAttribs->FindObject( "Xsec" );
               if (curAttr == 0) m_logger << kFATAL << "No \"Xsec\" attribute given" << Endl;
               TString xsecStr = curAttr->GetValue();
               
               Double_t Xsec = 0, eXsecStat = 0;// eXsecSyst = 0;
               std::vector<Double_t> eXsecSystVec;
               
               delete toklist;
               
               // replace "+-" by single sign that is simpler to tokenize 
               // (had problems with "+-" when cross section was negative)
               xsecStr.ReplaceAll( "+-", "@" );
               toklist = xsecStr.Tokenize( "@" );
               
               TString *tempStr = new TString( systError ); 
               tempStr->ReplaceAll( "+-", "#");
               TObjArray* toklistSystError = tempStr->Tokenize( "#" );
               TObjArray* toklistIndividualSystError = tempStr->Tokenize( "#" );
               tempStr->ReplaceAll( "#", "" ); 
               // search the systematic error to be reduced. ( ONLY ONE INDIVIDUAL SYSTEMATIC ERROR CAN BE REDUCED IN THIS WAY !!! ) 
               Int_t iSystWithMq = -1, nSystWithMq3p = -1, nSystWithMq2p = -1, nSystWithMq1p = -1, iCountIndividual = -1, isAsymmetric = 0;
               gStore().AssertVariable( "ComplexMerger::UseMaxSystSymmetrization" );
               Bool_t useMaxSystSymmetrization = gStore().GetVariable( "ComplexMerger::UseMaxSystSymmetrization" )->GetBoolValue();
               for( Int_t i=0; i<toklistSystError->GetEntries(); i++ ){
                  TString *toklistSystErrorStrAti = new TString( ((TObjString*)toklistSystError->At(i))->GetString() );
                  if( toklistSystErrorStrAti->Contains( "Individual" ) ){ iCountIndividual++; } 
                  
                  if( toklistSystErrorStrAti->Contains( "Mq3p" ) || toklistSystErrorStrAti->Contains( "Mq2p" ) || toklistSystErrorStrAti->Contains( "Mq1p" ) ){
                     if( iSystWithMq != (-1) ){ 
                        m_logger << kFATAL << "Several systematics (components) with quadratic subtraction were found !!!" << Endl; 
                     } 
                     if( !toklistSystErrorStrAti->Contains( "Individual" ) ){ 
                        m_logger << kFATAL << "Subtraction from a syst component not given individually !!!" << Endl; 
                     } 
                     iSystWithMq = iCountIndividual; 
                     toklistSystErrorStrAti->ReplaceAll( "Mq3p", "@" ); 
                     nSystWithMq3p = toklistSystErrorStrAti->CountChar( '@' ); 
                     toklistSystErrorStrAti->ReplaceAll( "@", "" ); 
                     
                     toklistSystErrorStrAti->ReplaceAll( "Mq2p", "@" ); 
                     nSystWithMq2p = toklistSystErrorStrAti->CountChar( '@' ); 
                     toklistSystErrorStrAti->ReplaceAll( "@", "" ); 
                     
                     toklistSystErrorStrAti->ReplaceAll( "Mq1p", "@" ); 
                     nSystWithMq1p = toklistSystErrorStrAti->CountChar( '@' ); 
                     toklistSystErrorStrAti->ReplaceAll( "@", "" ); 
                  }
                  // sanity check 
                  if( toklistSystErrorStrAti->Contains( "Mq" ) ){ 
                     m_logger << kFATAL << "Unknown Individual subtraction !!!" << Endl; 
                  }
                  if( toklistSystErrorStrAti->Contains("AsymSyst") && (!toklistSystErrorStrAti->Contains("Individual")) ){
                     m_logger << kFATAL << "Treatment of asymmetric errors implemented only for individual errors: symmetrization!!!" << Endl;
                  }
                  if( toklistSystErrorStrAti->Contains("AsymSyst") ){
		    //   m_logger << kWARNING << "Asymmetric systematic uncertainty: a symmetrization will be performed!" << Endl;
                     isAsymmetric = 1;
                  }
                  
                  delete toklistSystErrorStrAti;
               }
               
               for( Int_t i=0; i<toklistIndividualSystError->GetEntries(); ){
                  if( !(((TObjString*)toklistIndividualSystError->At(i))->GetString().Contains("Individual")) ){
                     toklistIndividualSystError->RemoveAt( i );
                     toklistIndividualSystError->Compress();
                  }
                  else{
                     i++;
                  }
               }
               /*
               if( isAsymmetric && toklistIndividualSystError->GetEntries()!=toklistSystError->GetEntries() ){
                  m_logger << kFATAL << "Symmetrization not implemented in presence of systematics given by formulas !!!  " << toklistIndividualSystError->GetEntries() << "  " << toklistSystError->GetEntries() << Endl;
               }
               */
               delete toklistSystError;
               
               tempStr->ReplaceAll( "#", "" );
               tempStr->ReplaceAll( "Individual", "@");
               Int_t nIndividual = tempStr->CountChar( '@' ); // count the number of errors given point by point 
               if( nIndividual != (iCountIndividual+1) ){ m_logger << kFATAL << "Problem in error counting !!!" << Endl; } 
               delete tempStr;
               
               if (toklist->GetEntries() == 2) {
                  Xsec      = ((TObjString*)toklist->At(0))->GetString().Atof();
                  eXsecStat = ((TObjString*)toklist->At(1))->GetString().Atof();

                  // sanity check
                  if ( systError.Contains( "Individual" ) && nIndividual!=0 ) {
                     m_logger << kFATAL << "Problem with Xsec syst error attribute in Channel: " << chName 
                              << " Exp: " << experiment << ": individual errors do not seem to be given" << " ["
                              << ((TObjString*)toklist->At(0))->GetString() << ", "
                              << ((TObjString*)toklist->At(1))->GetString() << "], "
                              << "but systError field has value: \"" << systError << "\"" << Endl;
                  }
               }
               else
                  if (toklist->GetEntries() >= 3) {
                     // sanity check
                     if ( (toklist->GetEntries()) != (nIndividual + 2) ) {
                        m_logger << kFATAL << "Problem with Xsec syst error attribute in Channel: " << chName 
                                 << " Exp: " << experiment << ": individual errors seem to be given" << " ["
                                 << ((TObjString*)toklist->At(0))->GetString() << ", "
                                 << ((TObjString*)toklist->At(1))->GetString() << ", "
                                 << ((TObjString*)toklist->At(2))->GetString() << "], "
                                 << " but systError field has value: \"" << systError << "\"" << Endl;
                     }
                     
                     Xsec      = ((TObjString*)toklist->At(0))->GetString().Atof();
                     eXsecStat = ((TObjString*)toklist->At(1))->GetString().Atof();
                     
                     Double_t newSyst = 0., totPosErr = 0., totNegErr = 0.;
                     for(Int_t i=2; i<(toklist->GetEntries()); i++){
                        
                        if( ((TObjString*)toklistIndividualSystError->At(i-2))->GetString().Contains("AsymSyst") && ( (!(((TObjString*)toklist->At(i))->GetString()).Contains("+")) || (!(((TObjString*)toklist->At(i))->GetString()).Contains("-")) ) ){
                           m_logger << kFATAL << "Asymmetric error announced, but not provided!" << Endl;
                        }
                        if( (!((TObjString*)toklistIndividualSystError->At(i-2))->GetString().Contains("AsymSyst")) && ( (((TObjString*)toklist->At(i))->GetString()).Contains("+") || (((TObjString*)toklist->At(i))->GetString()).Contains("-")) ){
                           m_logger << kFATAL << "No asymmetric error announced!  " << ((TObjString*)toklistIndividualSystError->At(i-2))->GetString() << "   " << ((TObjString*)toklist->At(i))->GetString() << Endl;
                        }
                        
                        if( !((TObjString*)toklistIndividualSystError->At(i-2))->GetString().Contains("AsymSyst") ){
                           if( (((TObjString*)toklist->At(i))->GetString()).Contains( "%" ) ){
                              // Sam Meehan : This version did not compile with an AB release because of a const error
                              // no functionality change should occur because of the following implementation
                              //newSyst = Xsec * (((TObjString*)toklist->At(i))->GetString().ReplaceAll( "%" , "" ).Atof())/100.;
                              
                              TString *toklistString = new TString( ((TObjString*)toklist->At(i))->GetString() );
                              newSyst = Xsec * (toklistString->ReplaceAll( "%" , "" ).Atof())/100.;                              
                           }
                           else{
                              newSyst = ((TObjString*)toklist->At(i))->GetString().Atof();
                           }
                           totPosErr += pow(newSyst,2);
                           totNegErr += pow(newSyst,2);
                        }
                        else{
                           TObjArray* tokListAsymSyst = ((TObjString*)toklist->At(i))->GetString().Tokenize( "-" );
                           if( !((TObjString*)tokListAsymSyst->At(0))->GetString().Contains("+") || ((TObjString*)tokListAsymSyst->At(1))->GetString().Contains("+") ){
                              m_logger << kFATAL << "Problem in asymmetric error format !!!" << Endl;
                           }
                           
                           Double_t posSyst = 0., negSyst = 0.;
                           if( ((TObjString*)tokListAsymSyst->At(0))->GetString().Contains("%") ){
                              // Sam Meehan : This version did not compile with an AB release because of a const error
                              // no functionality change should occur because of the following implementation
                              //posSyst = Xsec * (((TObjString*)tokListAsymSyst->At(0))->GetString().ReplaceAll("+","").Atof())/100.;
                              
                              TString *tokListAsymSystString = new TString( ((TObjString*)tokListAsymSyst->At(0))->GetString() );
                              posSyst = Xsec * (tokListAsymSystString->ReplaceAll("+","").Atof())/100.;
                           }
                           else{
                              // Sam Meehan : This version did not compile with an AB release because of a const error
                              // no functionality change should occur because of the following implementation
                              //posSyst = ((TObjString*)tokListAsymSyst->At(0))->GetString().ReplaceAll("+","").Atof();
                              
                              TString *tokListAsymSystString = new TString( ((TObjString*)tokListAsymSyst->At(0))->GetString() );
                              posSyst = tokListAsymSystString->ReplaceAll("+","").Atof();
                           }
                           if( ((TObjString*)tokListAsymSyst->At(1))->GetString().Contains("%") ){
                              negSyst = Xsec * (((TObjString*)tokListAsymSyst->At(1))->GetString().Atof())/100.;
                           }
                           else{
                              negSyst = ((TObjString*)tokListAsymSyst->At(1))->GetString().Atof();
                           }
                           delete tokListAsymSyst;
                           
                           if( useMaxSystSymmetrization ){
                              newSyst = TMath::Max( posSyst, negSyst );
                              totPosErr += pow(newSyst,2);
                              totNegErr += pow(newSyst,2);
                           }
                           else{
                              newSyst = ( posSyst + negSyst )/2.;
                              totPosErr += pow(posSyst,2);
                              totNegErr += pow(negSyst,2);
                           }
                        }
                        if ( iSystWithMq == (i-2) ){ 
                           Double_t newSyst_ = newSyst; 
                           newSyst = TMath::Sqrt( pow(newSyst,2) - nSystWithMq3p*pow(0.03*Xsec,2) ); 
                           m_logger << kINFO << "Automatic quadratic reduction of the systematic uncertainty (Mq3p) X " << nSystWithMq3p << " " << Xsec << " " << systError << " " << newSyst_ << " " << newSyst << Endl; 
                           newSyst = TMath::Sqrt( pow(newSyst,2) - nSystWithMq2p*pow(0.02*Xsec,2) ); 
                           m_logger << kINFO << "Automatic quadratic reduction of the systematic uncertainty (Mq2p) X " << nSystWithMq2p << " " << Xsec << " " << systError << " " << newSyst << Endl; 
                           newSyst = TMath::Sqrt( pow(newSyst,2) - nSystWithMq1p*pow(0.01*Xsec,2) ); 
                           m_logger << kINFO << "Automatic quadratic reduction of the systematic uncertainty (Mq1p) X " << nSystWithMq1p << " " << Xsec << " " << systError << " " << newSyst << Endl; 
                        }
                        eXsecSystVec.push_back( newSyst );
                     }
                     totPosErr = sqrt( totPosErr + pow(eXsecStat,2) );
                     totNegErr = sqrt( totNegErr + pow(eXsecStat,2) );

                     // Shift nominal value if systematics are symmetrized (not performed if the maximum systematic is used)
                     if( !useMaxSystSymmetrization ){
                        Xsec += ( totPosErr - totNegErr )/2.;
                     }
                     m_logger << " Xsec = " << Xsec << "   Total Error: +" << totPosErr << " -" << totNegErr << Endl;
                  }
                  else{
                     m_logger << kFATAL << "Problems in Xsec attribut of Data: " << toklist->GetEntries()
                              << Endl;
                  }
               
	       delete toklistIndividualSystError;
               
               // create new Data object

	       // sanity check
               if ( eXsecStat < 0 ){
                  m_logger << kFATAL << "eXsecStat = " << eXsecStat << " found in the experiment " << experiment << " while Xsec = " << Xsec << Endl;
               }
               
               if( systError == "None" )
                  eXsecSystVec.clear() ;
               
               Data* data  = new Data( Emin, Emax, Xsec, eXsecStat, eXsecSystVec );
               // and add to measurement 
	       measurement->AddData( data ); 
	    } // end of data iterator
         
         
         // unable to use splines if only one data in the measurement
         if( measurement->GetDataVec().size() <= 1 ){
            measurement->SetUseSplines( kFALSE );
         }
         
         // if active, print the evaluation mode of the measurement
         if( ((UnitMeasurement*)measurement)->IsActive() ){
            ((UnitMeasurement*)measurement)->PrintMeasurementStatus();
         }
         
         // add measurement to channel
         if (active == "T"){
            channel->AddMeasurement( measurement );  
	 }
      }
   } // end of measurement iterator
   
   // add channel to global data holder
   gData().AddChannel( channel );
   
   // sanity check
   if (gData().GetChannelVec().size() == 0) {
      m_logger << kFATAL << "Global data object does not contain any channels ?!" << Endl;
   }
}
