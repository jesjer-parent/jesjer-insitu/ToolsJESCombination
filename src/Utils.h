// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Utils_h
#define Utils_h

#include <vector>

#include "TROOT.h"
#include "TMatrixD.h"
#include "TArrayD.h"

class TList;
class TString;
class MsgLogger;
class TRandom3;
class TH1;
class TGraph;
class Interval;

namespace Utils {
   
   TList* ParseFormatLine( const TString& formatString, const TString& separator );
      
   // routines for formatted output -----------------
   void FormattedOutput( const std::vector<Double_t>&, const std::vector<TString>&, 
                         const TString titleVars, const TString titleValues, MsgLogger& logger,
                         TString format = "%+1.3f" );
   void FormattedOutput( const TMatrixD&, const std::vector<TString>&, MsgLogger& logger );

   // computes asymmetric uncertainties from distribution of toys
   void ComputeAsymmetricUncertainties( Int_t Ntoy_, TVectorD *toy_vec_, Double_t nominal_, Int_t &Npos_, Int_t &Nneg_, Double_t &sigmaPos_, Double_t &sigmaNeg_ );

   // turns covariance into correlation matrix
   const TMatrixD* GetCorrelationMatrix( const TMatrixD& covMat, MsgLogger& );

   // color settings
   const TString& Color( const TString& color );

  // binary search function taking into account the precision 
  // the TMath method doesn't work propertly
  Int_t BinarySrc(Int_t n, const Double_t *array, Double_t value);
  Int_t BinarySrc(Int_t n, const std::vector<Double_t>& array, Double_t value);

   // toy generation
   void      GenGaussRnd  ( TArrayD& v, const Int_t size, TRandom3& );
  // select from a vector of real numbers the part corresponding to a vector of names
  void SelectCoeff( TArrayD& coeffMeasurement, std::vector<TString*>& systMeasurement, const TArrayD& coeff, const std::vector<TString*>& syst );

   // histogram treatment
   TH1*      GetHistogramFromGraph( const TString& name, const TString& title, const TGraph& graph, Int_t nbin );
   TH1*      GetHistogramFromGraph( const TString& name, const TString& title, const TGraph& graph, const TH1&  );
   TH1*      GetHistogramFromGraph( const TString& name, const TString& title, const TGraph& graph, Int_t nBinsPFinalBin, TH1* rms );

   // string cleaning (e.g., removal of blanks)
   void CleanString( TString& );

   // check if token is in token list
   Bool_t HasToken( const TString& str, const TString& separator );

   Interval* GetGraphLimits( const TGraph& );

   // print upon ROOT startup
   void WelcomeMessage();
}

#endif
