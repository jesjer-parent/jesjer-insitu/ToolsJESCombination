// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TObjArray.h"
#include "TObjString.h"
#include "TString.h"

#include "StringList.h"

StringList::StringList( const TString& argstr )
   : GBase( "StringList" ),
     m_args( *new std::vector<TString>() )
{
   m_args.clear();
   TString s = argstr;
   TObjArray* ar = s.Tokenize( ":" );
   for (Int_t i=0; i<ar->GetEntries(); i++) m_args.push_back( ((TObjString*)ar->At(i))->GetString() );
   delete ar;
}

StringList::StringList( std::vector<TString>& args ) 
   : GBase( "StringList" ),
     m_args( args )
{}

StringList::~StringList()
{}

Bool_t StringList::HasArg( const TString& str )  const 
{
   std::vector<TString>::iterator it;
   for (it = m_args.begin(); it != m_args.end(); it++) if ((*it) == str) return kTRUE;
   return kFALSE;
}

const TString& StringList::GetArg( Int_t iarg ) const
{
   if (iarg > (Int_t)m_args.size()-1 || iarg < 0) {
      m_logger << kFATAL << "Mismatch in StringList arg: " << iarg << " > " << (Int_t)m_args.size()-1 << Endl;
   }
   
   return m_args[iarg];
}

const TString StringList::GetArgStr() const
{
   TString argstr = "";
   std::vector<TString>::iterator it = m_args.begin();
   for (; it != m_args.end(); it++) {
      if (it != m_args.begin()) argstr += ":";
      argstr += *it;
   }   

   return argstr;
}
