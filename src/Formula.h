// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Formula_h
#define Formula_h

#include <vector>
#include "GBase.h"

class Formula : public GBase {

 public:

   Formula( const TString& theFormulaExpression );
   virtual ~Formula();
      
   // evaluate formula
   virtual Double_t Eval( std::vector<Double_t>& pars ) const = 0;

   // accessors
   const TString& GetExpression() const { return m_expression; }

 protected:

   virtual void ParseExpression() = 0;

   TString m_expression;

};

#endif
