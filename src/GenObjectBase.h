// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef GenObjectBase_h
#define GenObjectBase_h

#include "GBase.h"

class TDirectory;

template <class T>
class GenObjectBase : public GBase {

 public:

   // default and copy constructors
   GenObjectBase();
   GenObjectBase( const GenObjectBase& other );

   // destructor
   virtual ~GenObjectBase();

   // toy cycle
   virtual void Init();
   virtual void Update( const T& ) = 0;
   virtual void Finalise();

   virtual void Write( TDirectory* dir = 0 ) = 0;
   virtual void PrintResults() const = 0;

   // original references
   virtual void     SetReference( const T& ) = 0;
   virtual const T* GetReference() const = 0;

   // counter
   Int_t GetCounter() const { return m_ncount; }
   void  IncrementCounter() { m_ncount++; }
   void  ResetCounter    () { m_ncount = 0; }

   // init and final flags
   Bool_t IsFinalised () const { return m_finalised; }
   void SetFinalised  () { m_finalised = kTRUE; }
   void UnsetFinalised() { m_finalised = kFALSE; }

 protected:

 private:

   // toy counter
   Int_t   m_ncount;

   // status flags (initialised already in GBase)
   Bool_t  m_finalised;
};

template <class T>
GenObjectBase<T>::GenObjectBase() 
   : GBase( "GenObjectBase<T>" ), 
     m_ncount( 0 ), 
     m_finalised( kFALSE ) 
{}

template <class T>
GenObjectBase<T>::GenObjectBase( const GenObjectBase& other )
   : GBase( other ), 
     m_ncount( other.m_ncount ), 
     m_finalised( other.m_finalised ) 
{}

// destructor
template <class T>
GenObjectBase<T>::~GenObjectBase() 
{}

// init toy cycle
template <class T>
inline void GenObjectBase<T>::Init() 
{ 
   // must call base class initialisation
   GBase::Init(); 

   ResetCounter(); 
   UnsetFinalised(); 
}

template <class T>
inline void GenObjectBase<T>::Finalise() 
{    
   if (IsFinalised()) return; // nothing to do
   if (GetCounter() <= 0) m_logger << kFATAL << "<Finalise> Zero counts" << Endl;
}

#endif
