// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TSpline1.h"
#include "MsgLogger.h"

//_______________________________________________________________________
TSpline1::TSpline1( TString title, TGraph* theGraph, Bool_t isHistogram, const std::vector<Double_t>& energySize )
  : TSplineBase( title, theGraph, isHistogram, energySize )
{ }

//_______________________________________________________________________
TSpline1::~TSpline1( void )
{ 
}

//_______________________________________________________________________
//there is NO vertical displacement in this evaluation (even for binned data)
Double_t TSpline1::Eval( Double_t x ) const 
{  
   // returns linearly interpolated TGraph entry around x
   Int_t ibin = Utils::BinarySrc( fGraph->GetN(),
                                     fGraph->GetX(),
                                     x );
   Int_t nbin = fGraph->GetN();

   // sanity checks
   if (ibin < 0    ) ibin = 0;
   if (ibin >= nbin) ibin = nbin - 1;

   Int_t nextbin = ibin;
   if ((x > fGraph->GetX()[ibin] && ibin != nbin-1) || ibin == 0) 
      nextbin++;
   else
      nextbin--;  

   // linear interpolation
   Double_t dx = fGraph->GetX()[ibin] - fGraph->GetX()[nextbin];
   Double_t dy = fGraph->GetY()[ibin] - fGraph->GetY()[nextbin];

   // if it's not a toy, test the limits of confidence of the splines
   if( ( !((TString)GetName()).Contains("Toy") ) && (fGraph->GetY()[ibin] + (x - fGraph->GetX()[ibin]) * dy/dx)<0. )
     *m_logger << kWARNING << "you might be looking for a value outside the limits of confidence of your splines in " <<  ((TString)GetName()) << " "
               << fGraph->GetY()[ibin] + (x - fGraph->GetX()[ibin]) * dy/dx << " " << nbin << " " << fGraph->GetY()[ibin] << " " << fGraph->GetX()[ibin] << " " << x << Endl;

   return fGraph->GetY()[ibin] + (x - fGraph->GetX()[ibin]) * dy/dx;
}

//-----------------------------------------------------------------------
// no vertical displacement of splines is considered here ( for a GRAPH of points )
Double_t TSpline1::EvalIntegralPoints( Double_t xmin, Double_t xmax ) const
{
  Double_t I = 0.;

  // test the order of the two values
  if( xmin > xmax )
    {
      *m_logger << kFATAL << "you have an xmin > xmax" <<Endl;
    }

   Int_t ibin1 = Utils::BinarySrc( fGraph->GetN(),
                                     fGraph->GetX(),
                                     xmin );
   Int_t ibin2 = Utils::BinarySrc( fGraph->GetN(),
                                     fGraph->GetX(),
                                     xmax );
   Int_t nbin = fGraph->GetN();

   // sanity checks
   if (ibin1 < 0    ) ibin1 = 0;
   if (ibin2 < 0    ) ibin2 = 0;
   if (ibin1 >= nbin) ibin1 = nbin - 1;
   if (ibin2 >= nbin) ibin2 = nbin - 1;

   // find the relative position of the two points and compute the integral
   // to be optimized if necessary
   if( (ibin1 == ibin2) || (ibin1 == nbin-2 && ibin2 == nbin-1) )
     {
       // the two points are on the same line
       // we want the confidence test to be done here for xmin and xmax
       I = ( xmax - xmin )*( Eval(xmin) + Eval(xmax) )/2.;
     }
   else
     {
       // the two points are on different lines

       // sanity check
       if( ibin1 > ibin2 )
         *m_logger << kFATAL << "problems in EvalIntegral" <<Endl;
         
       // add the integral from xmin to the next point
       I += ( fGraph->GetX()[ ibin1 + 1 ] - xmin )*Eval( ( xmin + fGraph->GetX()[ ibin1 + 1 ] )/2. );

       // add the integral from the points between xmax and xmin
       for( Int_t i=ibin1+1; i<ibin2 ; i++ )
         I += ( fGraph->GetX()[ i+1 ] - fGraph->GetX()[ i ] ) * (fGraph->GetY()[ i ] + fGraph->GetY()[ i+1 ])/2. ;

       // add the integral from the las point added before to xmax
       I += ( xmax - fGraph->GetX()[ ibin2 ] )*Eval( ( fGraph->GetX()[ ibin2 ] + xmax )/2. );
     }

   return I;
}

//------------------------------------------------------------------------------------
// this function should consider vertical displacements of splines ( for a HISTOGRAM )
Double_t TSpline1::EvalIntegralBins( Double_t xmin, Double_t xmax ) const
{
  // init the integral value
  Double_t I = 0.;

  // test the order of the two values
  if( xmin > xmax ){
    *m_logger << kFATAL << "you have an xmin > xmax" <<Endl;
  }

  // find the relative positions of xmin and xmax with respect to the CENTER of the bins
   Int_t ibin1 = Utils::BinarySrc( fGraph->GetN(),
                                   fGraph->GetX(),
                                   xmin );
   Int_t ibin2 = Utils::BinarySrc( fGraph->GetN(),
                                   fGraph->GetX(),
                                   xmax );
   Int_t nbin = fGraph->GetN();

   // sanity checks
   if (ibin1 < 0    ) ibin1 = 0;
   if (ibin2 < 0    ) ibin2 = 0;
   if (ibin1 >= nbin) ibin1 = nbin - 1;
   if (ibin2 >= nbin) ibin2 = nbin - 1;

   // find the relative positions of xmin and xmax with respect to the LIMITS of the bins
   // here we use one limit less than in TSpline2
   Int_t jbin1 = Utils::BinarySrc( eBinLimits.size()-1,
                                   eBinLimits,
                                   xmin );
   Int_t jbin2 = Utils::BinarySrc( eBinLimits.size()-1,
                                   eBinLimits,
                                   xmax );
   
   // sanity checks
   if (jbin1 < 0    ) jbin1 = 0;
   if (jbin2 < 0    ) jbin2 = 0;
   if (jbin1 >= nbin) jbin1 = nbin - 1;
   if (jbin2 >= nbin) jbin2 = nbin - 1;
   
   // sanity check
   if( nbin != (Int_t)eBinLimits.size()-1 ){
     *m_logger << kFATAL << "Not the same number of entries in theGraph: " << nbin << " and energySize vector: " << eBinLimits.size()-1 << Endl;
   }


   // find the relative position of the two points and compute the integral
   // if there are some entire bins between the two points, add their total integral

   if( ibin1 == ibin2 )
     {
       // the two points are on the same line
       // we want the confidence test to be done here for xmin and xmax

        if( xmin<eBinLimits.at(0) && xmax>eBinLimits.at(1) )
         {
           // sanity check
           if( ibin1 != 0)
             *m_logger << kFATAL << "Problems in TSpline1: bins" << Endl;

           I = ( xmax - eBinLimits.at(1) )*( Eval(xmax) + Eval(eBinLimits.at(1)) )/2.;
           I += fGraph->GetY()[0] * eBinSize.at(0);
           I += ( -xmin + eBinLimits.at(0) )*( Eval(xmin) + Eval(eBinLimits.at(0)) )/2.;
  
         }
       else
         I = ( xmax - xmin )*( Eval(xmin) + Eval(xmax) )/2.;

     }
   else
     if( (ibin1 == nbin-2) && (ibin2 == nbin-1) )
       {
         // the two points are on the same line (the last one)
          if( xmin<eBinLimits.at(nbin-1) && xmax>(eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)) )
           {
              I = ( xmax - (eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)) )*( Eval(xmax) + Eval(eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)) )/2.;
              I += fGraph->GetY()[nbin-1] * eBinSize.at(nbin-1);
              I += ( -xmin + eBinLimits.at(nbin-1) )*( Eval(xmin) + Eval(eBinLimits.at(nbin-1)) )/2.;
           }
         else
           I = ( xmax - xmin )*( Eval(xmin) + Eval(xmax) )/2.;
       }
     else
     {
       // the two points are on different lines

       // sanity check
       if( ibin1 > ibin2 )
         *m_logger << kFATAL << "problems in EvalIntegral" <<Endl;

       if( (jbin2-jbin1) <= 1 )
         { // the two points are in the same or neighbor bin(s)
           // use the "points" method
                   
           // add the integral from xmin to the next point
           I = ( fGraph->GetX()[ ibin1 + 1 ] - xmin )*Eval( ( xmin + fGraph->GetX()[ ibin1 + 1 ] )/2. );

           // add the integral from the points between xmax and xmin
           for( Int_t i=ibin1+1; i<ibin2 ; i++ )
             I += ( fGraph->GetX()[ i+1 ] - fGraph->GetX()[ i ] ) * (fGraph->GetY()[ i ] + fGraph->GetY()[ i+1 ])/2. ;

           // add the integral from the last point added before to xmax
           I += ( xmax - fGraph->GetX()[ ibin2 ] )*Eval( ( fGraph->GetX()[ ibin2 ] + xmax )/2. );
         }
       else
         {
           // use the "bins" method
           // add the integral from xmin to the upper limit of its bin
           if( xmin > fGraph->GetX()[jbin1] )
             {
               // xmin is in the right half of its bin
                I = ( eBinLimits.at(jbin1+1) - xmin )*Eval( (xmin + eBinLimits.at(jbin1+1))/2. );        
             }
           else
             {
               // xmin is in the left half of its bin
               I = ( fGraph->GetX()[jbin1] - xmin )*Eval( (xmin + fGraph->GetX()[jbin1])/2. );   
               I += ( eBinLimits.at(jbin1+1) - fGraph->GetX()[jbin1] )*Eval( (fGraph->GetX()[jbin1] + eBinLimits.at(jbin1+1))/2. );   
             }

           // add the integral from the bins between xmax and xmin
           for( Int_t j=jbin1+1; j<jbin2 ; j++ )
              I += (eBinSize.at(j)) * (fGraph->GetY()[ j ]) ;

           // add the integral from the bottom limit of the bin, to xmax
           if( xmax < fGraph->GetX()[jbin2] )
             {
               // xmax is in the left half of its bin
                I += ( -eBinLimits.at(jbin2) + xmax )*Eval( (xmax + eBinLimits.at(jbin2))/2. );        
             }
           else
             {
               // xmax is in the right half of its bin
                I += ( -eBinLimits.at(jbin2) + fGraph->GetX()[jbin2] )*Eval( (fGraph->GetX()[jbin2] + eBinLimits.at(jbin2))/2. );   
               I += ( -fGraph->GetX()[jbin2] + xmax )*Eval( (xmax + fGraph->GetX()[jbin2])/2. );   
             }

         }

     }

   if( !conserveBinIntegral_ ){ return I; }
   // Corrections of the integral

   // Compute the vertical displacements (to conserve integral for initial bins) and correct integral
   if( (xmin>eBinLimits.at(0)) && (xmax<eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)) )
     {// corrections at the two edges have to be considered
       if( jbin1==jbin2 )
         {
            Double_t sup =(jbin1==nbin-1)?(eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)):(eBinLimits.at(jbin1+1));
            return I - 0.25*( Eval(eBinLimits.at(jbin1)) + Eval( sup ) - 2*fGraph->GetY()[jbin1]) * (xmax-xmin);
         }
       else
         {
            I -= 0.25*( Eval(eBinLimits.at(jbin1)) + Eval(eBinLimits.at(jbin1+1)) - 2*fGraph->GetY()[jbin1] ) * ( eBinLimits.at(jbin1+1) - xmin );

            Double_t sup =(jbin2==nbin-1)?(eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)):(eBinLimits.at(jbin2+1));
            I -= 0.25*( Eval(eBinLimits.at(jbin2)) + Eval( sup ) - 2*fGraph->GetY()[jbin2] ) * ( xmax-eBinLimits.at(jbin2) );

           return I;
         }
     }
   
   // xmin or xmax is outside the bins
   if( (xmin>eBinLimits.at(0)) && (xmin<eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)))
     {
        Double_t sup = (jbin1==nbin-1)?(eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)):(eBinLimits.at(jbin1+1));
        return I - 0.25*( Eval(eBinLimits.at(jbin1)) + Eval( sup ) - 2*fGraph->GetY()[jbin1])*( sup-xmin );
     }

   if( (xmax>eBinLimits.at(0)) && (xmax<eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)))
     {
        Double_t sup = (jbin2==nbin-1)?(eBinLimits.at(nbin-1)+eBinSize.at(nbin-1)):(eBinLimits.at(jbin2+1));
        return I - 0.25*( Eval(eBinLimits.at(jbin2)) + Eval( sup ) - 2*fGraph->GetY()[jbin2])*( xmax-eBinLimits.at(jbin2) );
     }

  // xmin and xmax are outside the bins, and no correction is to be done
   return I;
}

// returns the integral of the spline1 between xmin and xmax of a graph of points
//this function should consider vertical displacements of splines, if necessary (for histograms)
Double_t TSpline1::EvalIntegral( Double_t xmin, Double_t xmax ) const
{
  if ( hasBins_ )
    { 
      // if we have bins compute the integral for a histogram
      return EvalIntegralBins( xmin, xmax );
    }

  // else compute the integral for a graph of points
  return EvalIntegralPoints( xmin, xmax );

}
