// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef XMLInterpreter_h
#define XMLInterpreter_h

#include "GBase.h"

class TString;

class XMLInterpreter : public GBase {
   
 public:
   
   XMLInterpreter( const TString& xmlFileName );
   ~XMLInterpreter();
   
   virtual Bool_t Interpret() const = 0;
   
 protected:
   
   const TString& GetXMLFileName() const { return m_xmlFileName; }

 private:

   TString m_xmlFileName;
};

#endif
