// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TString.h"

#include "XMLInterpreter.h"

XMLInterpreter::XMLInterpreter( const TString& xmlFileName )
   : GBase( "XMLInterpreter" ),
     m_xmlFileName( xmlFileName )
{}

XMLInterpreter::~XMLInterpreter()
{}
