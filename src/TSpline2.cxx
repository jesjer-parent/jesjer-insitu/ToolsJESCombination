// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TSpline2.h"
#include "MsgLogger.h"

//_______________________________________________________________________
TSpline2::TSpline2( TString title, TGraph* theGraph, Bool_t isHistogram, const std::vector<Double_t>& energySize )
   : TSplineBase( title, theGraph, isHistogram, energySize )
{ }

//_______________________________________________________________________
TSpline2::~TSpline2( void )
{ }

//_______________________________________________________________________
// returns quadratically interpolated TGraph entry around x
//there is NO vertical displacement in this evaluation (not even for binned data)
Double_t TSpline2::Eval( const Double_t x ) const 
{  
   Double_t retval=0;
   
   Int_t ibin = Utils::BinarySrc( fGraph->GetN(),
                                  fGraph->GetX(),
                                  x );

   // sanity checks
   if (ibin < 0               ) ibin = 0;
   if (ibin >= fGraph->GetN()) ibin =  fGraph->GetN() - 1;
  
   Double_t dx = 0; // should be zero
  
   if (ibin == 0 ) {
    
      retval = Quadrax(  x,
                         fGraph->GetX()[ibin]   + dx,
                         fGraph->GetX()[ibin+1] + dx,
                         fGraph->GetX()[ibin+2] + dx,
                         fGraph->GetY()[ibin],
                         fGraph->GetY()[ibin+1],
                         fGraph->GetY()[ibin+2]);
    
   }
   else if (ibin >= (fGraph->GetN()-2)) {
     if ( ibin == (fGraph->GetN()-2) )
       {
         ibin++;
       }
    
      retval = Quadrax( x, 
                        fGraph->GetX()[ibin-2] + dx,
                        fGraph->GetX()[ibin-1] + dx,
                        fGraph->GetX()[ibin]   + dx,
                        fGraph->GetY()[ibin-2],
                        fGraph->GetY()[ibin-1],
                        fGraph->GetY()[ibin]);
   }
   else {  
    
     retval =   Quadrax( x, 
                          fGraph->GetX()[ibin-1] + dx,
                          fGraph->GetX()[ibin]   + dx,
                          fGraph->GetX()[ibin+1] + dx,
                          fGraph->GetY()[ibin-1],
                          fGraph->GetY()[ibin],
                          fGraph->GetY()[ibin+1]) * 0.5
                +
                Quadrax( x, 
                          fGraph->GetX()[ibin] + dx,
                          fGraph->GetX()[ibin+1]  + dx,
                          fGraph->GetX()[ibin+2]  + dx,
                          fGraph->GetY()[ibin],
                          fGraph->GetY()[ibin+1],
                          fGraph->GetY()[ibin+2]) * 0.5;
   }

   //test the limits of confidence of the splines
   if( ( ((TString)GetName()) != "Toy") && (retval<0) )
     *m_logger << kWARNING << "you might be looking for a value outside the limits of confidence of your splines: " 
               << retval << Endl;

   return retval;
}

//_______________________________________________________________________
Double_t TSpline2::Quadrax( const Double_t dm,const Double_t dm1,const Double_t dm2,const Double_t dm3,
                            const Double_t cos1, const Double_t cos2, const Double_t cos3 ) const
{  

   Double_t a = cos1*(dm2-dm3) + cos2*(dm3-dm1) + cos3*(dm1-dm2);
   Double_t b = cos1*(dm2*dm2-dm3*dm3) + cos2*(dm3*dm3-dm1*dm1) + cos3*(dm1*dm1-dm2*dm2);
   Double_t c = cos1*(dm2-dm3)*dm2*dm3 + cos2*(dm3-dm1)*dm3*dm1 + cos3*(dm1-dm2)*dm1*dm2;

   Double_t denom = (dm2-dm3)*(dm3-dm1)*(dm1-dm2);

   // sanity check
   if ( TMath::Abs(denom) <1e-15 ){
     *m_logger << kFATAL << "Not a good set of points to use with TSpline2: " << dm1 << "  " << dm2 << "  " << dm3 << Endl;
   }
  
   return (-a*dm*dm+b*dm-c)/denom;
}

//_______________________________________________________________________
// computes the primitive of a quadratic interpolation
Double_t TSpline2::QuadraxPrimitive( const Double_t dm,const Double_t dm1,const Double_t dm2,const Double_t dm3,
                                     const Double_t cos1, const Double_t cos2, const Double_t cos3 ) const
{  
   Double_t a = cos1*(dm2-dm3) + cos2*(dm3-dm1) + cos3*(dm1-dm2);
   Double_t b = cos1*(dm2*dm2-dm3*dm3) + cos2*(dm3*dm3-dm1*dm1) + cos3*(dm1*dm1-dm2*dm2);
   Double_t c = cos1*(dm2-dm3)*dm2*dm3 + cos2*(dm3-dm1)*dm3*dm1 + cos3*(dm1-dm2)*dm1*dm2;

   Double_t denom = (dm2-dm3)*(dm3-dm1)*(dm1-dm2);

   if ( TMath::Abs(denom) <1e-15 )
     *m_logger << kFATAL << "Not a good set of points to use with TSpline2: " << dm1 << "  " << dm2 << "  " << dm3 << Endl;

   return ( -a*dm*dm*dm/3. + b*dm*dm/2. - c*dm )/denom;
}


//_______________________________________________________________________
//this function should NOT consider vertical displacements of splines ( for a GRAPH of points )
Double_t TSpline2::EvalIntegralPoints( Double_t xmin, Double_t xmax ) const
{
  Double_t I = 0.;

  // test the order of the two values
  if( xmin > xmax + 1e-15 )
    {
      *m_logger << kFATAL << " you have an (xmin=" << xmin << ") > (xmax=" << xmax << ")" <<Endl;
    }

   Int_t ibin1 = Utils::BinarySrc( fGraph->GetN(),
                                   fGraph->GetX(),
                                   xmin );
   Int_t ibin2 = Utils::BinarySrc( fGraph->GetN(),
                                   fGraph->GetX(),
                                   xmax );
   Int_t nbin = fGraph->GetN();

   // sanity checks
   if (ibin1 < 0    ) ibin1 = 0;
   if (ibin2 < 0    ) ibin2 = 0;
   if (ibin1 >= nbin) ibin1 = nbin - 1;
   if (ibin2 >= nbin) ibin2 = nbin - 1;

   Double_t dx = 0.; // should be zero

   // find the relative position of the two points and compute the integral
   if( (ibin1 == ibin2) || (ibin2 == nbin-1 && ibin1 == nbin-2) )
     {
       // the two points are given by the same function(s)
       // we want the confidence test to be done here for xmin and xmax

       if( (ibin2 == 0) || (ibin2 == nbin-1) || (ibin2 == nbin-2) )
         {
           if( (ibin2 == nbin-1) || (ibin2 == nbin-2) )
             {
               // if we are looking for an integral on the right side of the spline, we have to adjust coefficients
               ibin1 = nbin-3;
               ibin2 = nbin-3;
             }
           // one single functions is to be used
           I = QuadraxPrimitive(  xmax,
                                  fGraph->GetX()[ibin2]   + dx,
                                  fGraph->GetX()[ibin2+1] + dx,
                                  fGraph->GetX()[ibin2+2] + dx,
                                  fGraph->GetY()[ibin2],
                                  fGraph->GetY()[ibin2+1],
                                  fGraph->GetY()[ibin2+2])
               -
               QuadraxPrimitive(  xmin,
                                  fGraph->GetX()[ibin1]   + dx,
                                  fGraph->GetX()[ibin1+1] + dx,
                                  fGraph->GetX()[ibin1+2] + dx,
                                  fGraph->GetY()[ibin1],
                                  fGraph->GetY()[ibin1+1],
                                  fGraph->GetY()[ibin1+2]);
         }
       else
         {
           // sanity check
           if(ibin1!=ibin2)
             *m_logger << kFATAL << ibin1 << "  " << ibin2 <<Endl;

           // the average of two functions is to be used
           I =   QuadraxPrimitive( xmax, 
                                   fGraph->GetX()[ibin2-1] + dx,
                                   fGraph->GetX()[ibin2]   + dx,
                                   fGraph->GetX()[ibin2+1] + dx,
                                   fGraph->GetY()[ibin2-1],
                                   fGraph->GetY()[ibin2],
                                   fGraph->GetY()[ibin2+1]) * 0.5
                + 
                 QuadraxPrimitive( xmax, 
                                   fGraph->GetX()[ibin2] + dx,
                                   fGraph->GetX()[ibin2+1]  + dx,
                                   fGraph->GetX()[ibin2+2]  + dx,
                                   fGraph->GetY()[ibin2],
                                   fGraph->GetY()[ibin2+1],
                                   fGraph->GetY()[ibin2+2]) * 0.5
               -
                 QuadraxPrimitive( xmin, 
                                   fGraph->GetX()[ibin1-1] + dx,
                                   fGraph->GetX()[ibin1]   + dx,
                                   fGraph->GetX()[ibin1+1] + dx,
                                   fGraph->GetY()[ibin1-1],
                                   fGraph->GetY()[ibin1],
                                   fGraph->GetY()[ibin1+1]) * 0.5
               - 
                 QuadraxPrimitive( xmin, 
                                   fGraph->GetX()[ibin1] + dx,
                                   fGraph->GetX()[ibin1+1]  + dx,
                                   fGraph->GetX()[ibin1+2]  + dx,
                                   fGraph->GetY()[ibin1],
                                   fGraph->GetY()[ibin1+1],
                                   fGraph->GetY()[ibin1+2]) * 0.5;
         }
     }
   else
     {
       // the two points are NOT given by the same function(s)

       // sanity check
       if( ibin1 > ibin2 )
         *m_logger << kFATAL << "problems in EvalIntegral" << Endl;

       // "normal" method to compute the integral
       // add the integral from xmin to the next point
       if( ibin1 == 0 )
         { 
           // one single function is used
           I += QuadraxPrimitive( fGraph->GetX()[ibin1+1],
                                  fGraph->GetX()[ibin1]   + dx,
                                  fGraph->GetX()[ibin1+1] + dx,
                                  fGraph->GetX()[ibin1+2] + dx,
                                  fGraph->GetY()[ibin1],
                                  fGraph->GetY()[ibin1+1],
                                  fGraph->GetY()[ibin1+2])
               -
               QuadraxPrimitive(  xmin,
                                  fGraph->GetX()[ibin1]   + dx,
                                  fGraph->GetX()[ibin1+1] + dx,
                                  fGraph->GetX()[ibin1+2] + dx,
                                  fGraph->GetY()[ibin1],
                                  fGraph->GetY()[ibin1+1],
                                  fGraph->GetY()[ibin1+2]);
         }
       else
         {
           // the average of two functions is to be used
           I +=  QuadraxPrimitive( fGraph->GetX()[ibin1+1], 
                                    fGraph->GetX()[ibin1-1] + dx,
                                    fGraph->GetX()[ibin1]   + dx,
                                    fGraph->GetX()[ibin1+1] + dx,
                                    fGraph->GetY()[ibin1-1],
                                    fGraph->GetY()[ibin1],
                                    fGraph->GetY()[ibin1+1]) * 0.5
               + 
                 QuadraxPrimitive( fGraph->GetX()[ibin1+1], 
                                   fGraph->GetX()[ibin1] + dx,
                                   fGraph->GetX()[ibin1+1]  + dx,
                                   fGraph->GetX()[ibin1+2]  + dx,
                                   fGraph->GetY()[ibin1],
                                   fGraph->GetY()[ibin1+1],
                                   fGraph->GetY()[ibin1+2]) * 0.5
               -
                 QuadraxPrimitive( xmin, 
                                   fGraph->GetX()[ibin1-1] + dx,
                                   fGraph->GetX()[ibin1]   + dx,
                                   fGraph->GetX()[ibin1+1] + dx,
                                   fGraph->GetY()[ibin1-1],
                                   fGraph->GetY()[ibin1],
                                   fGraph->GetY()[ibin1+1]) * 0.5
               - 
                 QuadraxPrimitive( xmin, 
                                   fGraph->GetX()[ibin1] + dx,
                                   fGraph->GetX()[ibin1+1]  + dx,
                                   fGraph->GetX()[ibin1+2]  + dx,
                                   fGraph->GetY()[ibin1],
                                   fGraph->GetY()[ibin1+1],
                                   fGraph->GetY()[ibin1+2]) * 0.5;
         }

       //find the point before which we have to consider, for sure, the average of two functions
       Int_t before_xmax = 0;
       if( (ibin2 == nbin-1) || (ibin2 == nbin-2) )
         before_xmax = nbin-2;
       else
         before_xmax = ibin2;

       // add the integral from the average of two functions (points between xmax and xmin)
       for( Int_t i=ibin1+1; i<before_xmax; i++ )
         {
           //the average of two functions is to be used
           I +=   QuadraxPrimitive( fGraph->GetX()[i+1], 
                                    fGraph->GetX()[i-1] + dx,
                                    fGraph->GetX()[i]   + dx,
                                    fGraph->GetX()[i+1] + dx,
                                    fGraph->GetY()[i-1],
                                    fGraph->GetY()[i],
                                    fGraph->GetY()[i+1]) * 0.5
               + 
                  QuadraxPrimitive( fGraph->GetX()[i+1], 
                                    fGraph->GetX()[i] + dx,
                                    fGraph->GetX()[i+1]  + dx,
                                    fGraph->GetX()[i+2]  + dx,
                                    fGraph->GetY()[i],
                                    fGraph->GetY()[i+1],
                                    fGraph->GetY()[i+2]) * 0.5
               -
                  QuadraxPrimitive( fGraph->GetX()[i], 
                                    fGraph->GetX()[i-1] + dx,
                                    fGraph->GetX()[i]   + dx,
                                    fGraph->GetX()[i+1] + dx,
                                    fGraph->GetY()[i-1],
                                    fGraph->GetY()[i],
                                    fGraph->GetY()[i+1]) * 0.5
               -
                  QuadraxPrimitive( fGraph->GetX()[i], 
                                    fGraph->GetX()[i] + dx,
                                    fGraph->GetX()[i+1]  + dx,
                                    fGraph->GetX()[i+2]  + dx,
                                    fGraph->GetY()[i],
                                    fGraph->GetY()[i+1],
                                    fGraph->GetY()[i+2]) * 0.5;


         }

       // add the integral from the las point added before to xmax
       if( (ibin2 == nbin-1) || (ibin2 == nbin-2) )
         {
           // we are looking for an integral on the right side of the spline, so we have to adjust coefficients (in order to use the same formulas as before)
           ibin2 = nbin-3;
           
           // one single functions is to be used
           I += QuadraxPrimitive(  xmax,
                                   fGraph->GetX()[ibin2]   + dx,
                                   fGraph->GetX()[ibin2+1] + dx,
                                   fGraph->GetX()[ibin2+2] + dx,
                                   fGraph->GetY()[ibin2],
                                   fGraph->GetY()[ibin2+1],
                                   fGraph->GetY()[ibin2+2])
             -
             QuadraxPrimitive(  fGraph->GetX()[before_xmax],
                                fGraph->GetX()[ibin2]   + dx,
                                fGraph->GetX()[ibin2+1] + dx,
                                fGraph->GetX()[ibin2+2] + dx,
                                fGraph->GetY()[ibin2],
                                fGraph->GetY()[ibin2+1],
                                fGraph->GetY()[ibin2+2]);
         }
       else
         {
           //the average of two functions is to be used
           I +=   QuadraxPrimitive( xmax, 
                                    fGraph->GetX()[ibin2-1] + dx,
                                    fGraph->GetX()[ibin2]   + dx,
                                    fGraph->GetX()[ibin2+1] + dx,
                                    fGraph->GetY()[ibin2-1],
                                    fGraph->GetY()[ibin2],
                                    fGraph->GetY()[ibin2+1]) * 0.5
               + 
                  QuadraxPrimitive( xmax, 
                                    fGraph->GetX()[ibin2] + dx,
                                    fGraph->GetX()[ibin2+1]  + dx,
                                    fGraph->GetX()[ibin2+2]  + dx,
                                    fGraph->GetY()[ibin2],
                                    fGraph->GetY()[ibin2+1],
                                    fGraph->GetY()[ibin2+2]) * 0.5
               -
                  QuadraxPrimitive( fGraph->GetX()[ibin2], 
                                    fGraph->GetX()[ibin2-1] + dx,
                                    fGraph->GetX()[ibin2]   + dx,
                                    fGraph->GetX()[ibin2+1] + dx,
                                    fGraph->GetY()[ibin2-1],
                                    fGraph->GetY()[ibin2],
                                    fGraph->GetY()[ibin2+1]) * 0.5
               - 
                  QuadraxPrimitive( fGraph->GetX()[ibin2], 
                                    fGraph->GetX()[ibin2] + dx,
                                    fGraph->GetX()[ibin2+1]  + dx,
                                    fGraph->GetX()[ibin2+2]  + dx,
                                    fGraph->GetY()[ibin2],
                                    fGraph->GetY()[ibin2+1],
                                    fGraph->GetY()[ibin2+2]) * 0.5;

         }


     }

   return I;
}

// this function should consider vertical displacements of splines ( for a HISTOGRAM )
Double_t TSpline2::EvalIntegralBins( Double_t xmin, Double_t xmax ) const
{
  // init the integral value
  Double_t I = 0.;

  // test the order of the two values
  if( xmin > xmax + 1e-15 ){
    *m_logger << kFATAL << " you have an (xmin=" << xmin << ") > (xmax=" << xmax << ")" <<Endl;
  }

   Int_t nbin = fGraph->GetN();
   // find the relative positions of xmin and xmax with respect to the LIMITS of the bins
   Int_t jbin1 = Utils::BinarySrc( eBinLimits.size(),
                                   eBinLimits,
                                   xmin );
   Int_t jbin2 = Utils::BinarySrc( eBinLimits.size(),
                                   eBinLimits,
                                   xmax );
   
   // sanity checks (changed for this function !!! )
   if (jbin1 < 0    ) jbin1 = -1;
   if (jbin2 < 0    ) jbin2 = -1;
   if (jbin1 >= nbin) jbin1 = nbin;
   if (jbin2 >= nbin) jbin2 = nbin;
   
   // sanity check
   if( jbin1 > jbin2 ) *m_logger << kFATAL << "problems in EvalIntegral" <<Endl;

   // sanity check
   if( nbin + 1 != (Int_t)eBinLimits.size() ){
     *m_logger << kFATAL << "In theGraph nbin = " << nbin << "  and the energySize vector has " << eBinLimits.size() << " entries "<< Endl;
   }

   if( (jbin2-jbin1) <= 1 ){ 
     // the two points are in the same or neighbor(maybe at the data limits) bin(s)
     // use the "points" method
     I += EvalIntegralPoints( xmin, xmax );
   }
   else{
     // use the "bins" method
     // add the integral from xmin to the upper limit of its bin
      I += EvalIntegralPoints( xmin, eBinLimits.at(jbin1+1) );
     
     // add the integral from the bins between xmax and xmin
     for( Int_t j=jbin1+1; j<jbin2 ; j++ ){
        I += (eBinSize.at(j)) * (fGraph->GetY()[ j ]) ;
     }

     // add the integral from the bottom limit of the bin, to xmax
     I += EvalIntegralPoints( eBinLimits.at(jbin2), xmax );
   }

   if( !conserveBinIntegral_ ){ return I; }
   // Corrections of the integral

   // Compute the vertical displacements (to conserve integral for initial bins) and correct integral
   if( (jbin1 > -1) && (jbin2 < nbin) )
     {// corrections at the two edges have to be considered
       if( jbin1==jbin2 ){
          return I + ( fGraph->GetY()[jbin1] - EvalIntegralPoints(eBinLimits.at(jbin1), eBinLimits.at(jbin1+1))/(eBinSize.at(jbin1)) ) * (xmax-xmin);
       }
       else{
          I += ( fGraph->GetY()[jbin1] - EvalIntegralPoints(eBinLimits.at(jbin1), eBinLimits.at(jbin1+1))/(eBinSize.at(jbin1)) ) * ( eBinLimits.at(jbin1+1) - xmin );

          I += ( fGraph->GetY()[jbin2] - EvalIntegralPoints(eBinLimits.at(jbin2), eBinLimits.at(jbin2+1))/(eBinSize.at(jbin2)) ) * ( xmax-eBinLimits.at(jbin2) );
         
         return I;
       }
     }
   
   // xmin or xmax is outside the bins
   if( (jbin1 > -1) && (jbin1 < nbin) ){ // xmax outside
      return I + ( fGraph->GetY()[jbin1] - EvalIntegralPoints(eBinLimits.at(jbin1), eBinLimits.at(jbin1+1))/(eBinSize.at(jbin1)) )*( eBinLimits.at(jbin1+1)-xmin );
   }

   if( (jbin2 > -1) && (jbin2 < nbin) ){ // xmin outside
      return I + ( fGraph->GetY()[jbin2] - EvalIntegralPoints(eBinLimits.at(jbin2), eBinLimits.at(jbin2+1))/(eBinSize.at(jbin2)) )*( xmax-eBinLimits.at(jbin2) );
   }

   // xmin and xmax are outside the bins and no correction is to be done
   return I;

}

// returns the integral of the spline2 between xmin and xmax
// this function should consider vertical displacements of splines, if necessary (for histograms)
Double_t TSpline2::EvalIntegral( Double_t xmin, Double_t xmax ) const
{
  if ( hasBins_ )
    { 
      // if we have bins compute the integral for a histogram
      return EvalIntegralBins( xmin, xmax );
    }

  // else compute the integral for a graph of points
  return EvalIntegralPoints( xmin, xmax );

}
