// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef Interval_h
#define Interval_h

#include "GBase.h"

class Interval;
std::ostream& operator << ( std::ostream& os, const Interval& interval );

class Interval : public GBase {
   
 public:
   
  Interval( Double_t min, Double_t max );
  Interval( const Interval& ); // copy constructor (cloning input GData)
  Interval( const Interval* ); // copy constructor (cloning input GData)
  ~Interval();

  void SetMinMax( Double_t min, Double_t max );
   
   // accessors 
   Double_t GetMin()   const { return m_min; }
   Double_t GetMax()   const { return m_max; }
   Double_t GetWidth() const { return m_max - m_min; }
   Double_t GetMean()  const { return 0.5*(m_min + m_max); }
   
   // tester methods
   Bool_t IsWithin( Double_t x )            const { return x >= m_min && x <= m_max; }
   Bool_t IsWithin( const Interval& other ) const { return m_min <= other.GetMin() && m_max >= other.GetMax(); }

   // testers 
   enum EStatus { kSmaller = 0, 
                  kOverlappingSmaller, 
                  kEmbracing, 
                  kIdentical, 
                  kWithin, 
                  kOverlappingLarger, 
                  kLarger,
                  kNumCases };       
   static const char* EStatusStr[kNumCases];       
   EStatus  GetEStatus( const Interval& ) const;
   Double_t GetFracInterv( const Double_t EMin, const Double_t EMax, Double_t &left, Double_t &right ) const;
   Double_t GetFracInterv( const Double_t EMin, const Double_t EMax ) const;

 private:

   Double_t m_min;
   Double_t m_max;
};

#endif
