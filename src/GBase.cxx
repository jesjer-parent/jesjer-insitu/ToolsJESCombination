// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "MsgLogger.h"
#include "GBase.h"

ClassImp(GBase)

GBase::GBase( const TString& name ) 
   : m_name       ( name ),
     m_initialised( kFALSE ),
     m_logger     ( kINFO )
{
   m_logger.SetSource( m_name.Data() );
} 

GBase::GBase( const GBase& other )
   : m_name       ( other.m_name ),
     m_initialised( other.m_initialised ),
     m_logger     ( other.m_logger.GetSource() )
{}

GBase::~GBase() 
{}

