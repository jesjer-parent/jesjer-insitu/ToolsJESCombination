// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "TString.h"

#include "Action.h"

Action::Action( const TString& name, const TString& arg )
   : StringList( arg ),
     m_name( name ) 
{
     SetName( "Action" );
     ExtractActiveFlag();
 }
Action::Action( const TString& name, std::vector<TString>& args ) 
   : StringList( args ),
     m_name( name ) 
{
     SetName( "Action" );
     ExtractActiveFlag();
 }

void Action::ExtractActiveFlag()
{
  //first element in vector must be 
  if (GetEntries() < 1) {
      m_logger << kFATAL << "<ExtractActiveFlag> Wrong number of arguments: " << GetEntries() << Endl;
   }
  if (GetArg(0) != "T" && GetArg(0) != "F") {
      m_logger << kFATAL << "<ExtractActiveFlag> First argument must be boolean: \"" << GetArg(0) << "\"" << Endl;
   }

 m_active = GetArg(0) == "T";
 GetArgs().erase( std::vector<TString>::iterator( &GetArgs()[0] ) );
 }
 
