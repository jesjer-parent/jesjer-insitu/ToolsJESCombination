// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#include "GConfig.h"
#include "MsgLogger.h"

ClassImp(GConfig)

GConfig* GConfig::m_gConfigPtr = 0;

GConfig& gConfig() { return GConfig::Instance(); }

//_______________________________________________________________________
GConfig::GConfig() 
   : m_useColoredConsole( kTRUE ),
     m_logger( "GConfig" )
{
   // constructor - set defaults
}

//_______________________________________________________________________
GConfig::~GConfig()
{
   // destructor
}

