// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)

#ifndef GStore_h
#define GStore_h

#include <vector>
#include <map>

#include "GBase.h"
#include "Variable.h"

class TString;
class Action;

// forward declarations
class GStore;
GStore& gStore();

class GStore : public GBase {

 public:

   // singleton
   static GStore*  Instance();
   static GStore*  InitialInstance(); // returns object, without creating it
    
   // accessors
   const std::map<const TString, const Variable*>& GetVariables() const { return m_variables; }

   // actions
   const std::vector<const Action*>& GetActions() const { return m_actions; }
   void            AddAction( const TString action, std::vector<TString>& args ) ;
   void            AddAction( const Action* action );
   const Action*   GetAction( const TString& action );
   
   // global message level for output
   void            SetMsgLevel( MsgLevel mlevel ) { m_msgLevel = mlevel; }
   MsgLevel        GetMsgLevel() const { return m_msgLevel; }      

   // variables
   void            AssertVariable( const TString& key );
   Bool_t          ExistVariable( const TString& key );
   const Variable* GetVariable( const TString& key );
   const Variable* GetVariableIfExist( const TString& key );
   void            AddVariable( const Variable* );
   void            AddVariable( TString key, TString value );
   Bool_t          IsTrue( const TString& var ) {
      if (ExistVariable( var ) && GetVariable( var )->GetBoolValue()) return kTRUE;
      else return kFALSE;
   }

   // clear the full storage (delete all objects)
   void Clear();

   // print the store
   void Print( std::ostream& o ) const;

 private:

   GStore();
   ~GStore();

 private:

   // pointer to singleton instance
   static GStore* m_instance;

   // variables
   std::map<const TString, const Variable*> m_variables;

   // actions (note original ordering needs to be kept!)
   std::vector<const Action*> m_actions;

   // more stuff
   MsgLevel m_msgLevel;
};

#endif
