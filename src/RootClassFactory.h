// Authors: Nicolas Berger (LAPP-Annecy), Andreas Hoecker (CERN), Bogdan Malaescu (LPNHE-Paris)
//
// pure static factory class
//

#ifndef RootClassFactory_h
#define RootClassFactory_h

class TString;
class MsgLogger;

class RootClassFactory {
    
 public:

   static void* CreateObject( const TString& className, const TString& inheritsFrom = "" );

 private:

   static MsgLogger* m_logger;
};

#endif
