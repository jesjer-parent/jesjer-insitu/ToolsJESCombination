/*
 *  Utils.h -- a collection of helper functions for
 *             HEP analysis
 *
 *  Dag Gillberg
 *
 */

#ifndef Utils_h
#define Utils_h

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <bitset>
#include <time.h>

#include "TROOT.h"
#include "TSystem.h"
#include "TChain.h"
#include "TFile.h"
#include "TEnv.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TH2.h"
#include "TStyle.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TProfile2D.h"
#include "TGraphErrors.h"
#include "TLorentzVector.h"

using namespace std;

typedef vector<TString> StrV;
typedef vector<TH1D*> HistV;
typedef TString Str;
typedef unsigned int uint;
typedef vector<double> VecD;
typedef vector<float> VecF;
typedef vector<int> VecI;
typedef TGraphErrors Graph;

bool _verbose=false;

///////////////////////////////////////////////////////////////////////////////////////

void error(Str msg) {
  printf("ERROR:\n\n  %s\n\n",msg.Data()); 
  abort();
}

TFile *InitOutputFile(Str ofName) {
  if (!gSystem->AccessPathName(ofName))
    error(Str("The intended output file already exist!\n")+
          "  If you want to replace it, please delete it manually:\n\n"+
          "    rm "+ofName);
  printf("\nHitograms will be written to %s\n",ofName.Data());
  return new TFile(ofName,"CREATE");
}



map<Str,TH1F*> histos;
map<Str,TH2F*> histos2d;
map<Str,TH3F*> histos3d;
map<Str,TProfile*> profiles;
map<Str,TProfile2D*> profiles2d;

//**************************  methods for vectorizing input data *******************************//

void add(VecD &vec, double a) { vec.push_back(a); };
void add(VecD &vec, double a, double b) { add(vec,a); add(vec,b); };
void add(VecD &vec, double a, double b, double c) { add(vec,a,b); add(vec,c); };
void add(StrV &vec, Str a) { vec.push_back(a); };
void add(StrV &vec, Str a, Str b) { add(vec,a); add(vec,b); };
void add(StrV &vec, Str a, Str b, Str c) { add(vec,a,b); add(vec,c); };

map<Str,TFile*> _files;
TFile *OpenFile(Str fn) {
  TFile *f = _files[fn];
  //(TFile*)gROOT->FindObject(fn);
  if (f==NULL) f = TFile::Open(fn); _files[fn]=f;
  if (f==NULL) error("Cannot open "+fn); return f;
}

TH1D *GetHisto(TFile *f, Str hname) {
  if (f==NULL) error("GetHisto: file is NULL?? "); //+f->GetName());
  TH1D *h = (TH1D*)f->Get(hname);
  if (h==NULL) error("Cannot access "+hname+" in "+f->GetName());
  return h;
}

TH2D *Get2DHisto(TFile *f, Str hname) {
  if (f==NULL) error("Get2DHisto: file is NULL?? "); //+f->GetName());
  TH2D *h = (TH2D*)f->Get(hname);
  if (h==NULL) error("Cannot access "+hname+" in "+f->GetName());
  return h;
}

Graph *GetGraph(TFile *f, Str hname) {
  if (f==NULL) error("GetGraph: file is NULL?? "); //+f->GetName());
  Graph *g = (Graph*)f->Get(hname);
  if (g==NULL) error("Cannot access graph "+hname+" in "+f->GetName());
  return g;
}

void add(VecD &vec, double a[]) { 
  uint n=sizeof(a)/sizeof(a[0]);
  for (uint i=0;i<n;++i) vec.push_back(a[i]);
}

StrV Vectorize(Str str, Str sep=" ") {
  StrV result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) return result;
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr())
    if (os->GetString()[0]!='#') result.push_back(os->GetString());
    else break;
  return result;
}

VecD VectorizeD(Str str) {
  VecD result; StrV vecS = Vectorize(str);
  for (uint i=0;i<vecS.size();++i) 
    result.push_back(atof(vecS[i]));
  return result;
}

VecD MakeUniformVecD(int N, double min, double max) {
  VecD vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}

VecD MakeLogVector(int N, double min, double max) {
  VecD vec; double dx=(log(max)-log(min))/N;
  for (int i=0;i<=N;++i) vec.push_back(exp(log(min)+i*dx));
  return vec;
}


//********************* prepare histograms ***********//
 

void BookHisto(Str hname, Str tit, VecD bins, Str xtit="") {
  if (bins.size()==0) error("No bins specified for histo "+hname);
  TH1F *h = new TH1F(hname,tit,bins.size()-1,&bins[0]);
  h->SetXTitle(xtit); histos[hname]=h;
  if (_verbose) printf("Created 1D histo %s\n",hname.Data());
}

void BookHisto(Str hname, Str tit, int Nbins, double xmin, double xmax, Str xtit="") {
  BookHisto(hname,tit,MakeUniformVecD(Nbins,xmin,xmax),xtit);
}

void BookHisto2D(Str hname, Str tit, VecD xbins, VecD ybins, Str xtit="", Str ytit="") {
  if (xbins.size()==0) error("No xbins specified for histo "+hname);
  if (ybins.size()==0) error("No ybins specified for histo "+hname);
  TH2F *h = new TH2F(hname,tit,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  h->SetXTitle(xtit); h->SetYTitle(ytit); 
  histos2d[hname]=h;
  if (_verbose) printf("Created 2D histo %s\n",hname.Data());
}

void BookHisto3D(Str hname, Str tit, VecD xbins, VecD ybins, VecD zbins, Str xtit, Str ytit, Str ztit) {
  if (xbins.size()==0) error("No xbins specified for histo "+hname);
  if (ybins.size()==0) error("No ybins specified for histo "+hname);
  if (zbins.size()==0) error("No zbins specified for histo "+hname);
  TH3F *h = new TH3F(hname,tit,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0],zbins.size()-1,&zbins[0]);
  h->SetXTitle(xtit); h->SetYTitle(ytit); h->SetZTitle(ztit); 
  histos3d[hname]=h;
  if (_verbose) printf("Created 3D histo %s\n",hname.Data());
}


void BookProfile2D(Str hname, Str tit, VecD xbins, VecD ybins, Str xtit, Str ytit, Str ztit) {
  TProfile2D *p = new TProfile2D(hname,tit,xbins.size()-1,&xbins[0],ybins.size()-1,&ybins[0]);
  p->SetXTitle(xtit); p->SetYTitle(ytit); p->SetZTitle(ztit); 
  profiles2d[hname]=p;
  if (_verbose) printf("Created 2D profile %s\n",hname.Data());
}


//************************** histo filling ********************************************//


void FillHisto(Str hname, double x, double w=1.0) {
  TH1F *h=histos[hname]; if (h==NULL) error("Cannot find histo "+hname);
  h->Fill(x,w);
}

void FillHisto2D(Str hname, double x, double y, double w=1.0) {
  TH2F *h=histos2d[hname]; if (h==NULL) error("Cannot find 2D histo "+hname);
  h->Fill(x,y,w);
}

void FillHisto3D(Str hname, double x, double y, double z, double w=1.0) {
  TH3F *h=histos3d[hname]; if (h==NULL) error("Cannot find 3D histo "+hname);
  h->Fill(x,y,z,w);
}

void FillProfile2D(Str hname, double x, double y, double w=1.0) {
  TProfile2D *p=profiles2d[hname]; if (p==NULL) error("Cannot find 2D profile "+hname);
  p->Fill(x,y,w);
}

void FillHistoTwice(Str hname, double x1, double x2, double w=1.0) {
  FillHisto(hname,x1,w); FillHisto(hname,x2,w);
}


//********************* methods for opening and reading the config file as a TEnv ************//

TEnv *OpenSettingsFile(Str fileName) {
  if (fileName=="") error("No config file name specified. Cannot open file!");
  TEnv *settings = new TEnv();
  int status=settings->ReadFile(fileName.Data(),EEnvLevel(0));
  if (status!=0) error(Form("Cannot read file %s",fileName.Data()));
  return settings;
}

StrV ReadFile(TString fileName) {
  StrV lines;
  ifstream file(fileName.Data());
  string line, lastline="weeee";
  while (getline(file,line)) {
    if (line==lastline) continue;
    if (line[0]==' ') continue;
    StrV subLines=Vectorize(line,",");
    for (int i=0;i<(Int_t)subLines.size();++i) 
      lines.push_back(subLines[i]);
  }
  return lines;
}

//**************** cool method to keep track of runtimes ****************************//

Str getTime()
{
  time_t aclock;
  ::time( &aclock );
  return Str(asctime( localtime( &aclock )));
}



void PrintTime()
{
  static bool first=true;
  static time_t start;
  if(first) { first=false; ::time(&start); }
  time_t aclock; ::time( &aclock );
  char tbuf[25]; ::strncpy(tbuf, asctime( localtime( &aclock ) ),24);
  tbuf[24]=0;
  cout <<  "Current time: " << tbuf
       << " ( "<< ::difftime( aclock, start) <<" s elapsed )" << std::endl;
}


TH1D *MakeLogHisto(TString hname, int Nbins, double xmin, double xmax, TString xtit="") {
  VecD ptbins=MakeLogVector(Nbins,xmin,xmax);
  TH1D *h = new TH1D(hname,"",Nbins,&ptbins[0]); h->SetXTitle(xtit); return h;
}

TH1D *MakePtHisto(TString hname) {
  return MakeLogHisto(hname,100,15,2500,"#it{p}_{T} [GeV]");
}

TH1D *Add(vector<TH1D*> Histos_) {
  if (Histos_.size()==0) error("Bad input to Add");
  TH1D *tot = (TH1D*)Histos_[0]->Clone(Str(Histos_[0]->GetName())+"_sum");
  for (int i=1;i<(Int_t)Histos_.size();++i) {
    TH1D *h = Histos_[i];
    if (tot->GetNbinsX()!=h->GetNbinsX()) error("Bad binning");
    for (int bi=1;bi<=tot->GetNbinsX();++bi) {
      double y1=tot->GetBinContent(bi), y2=h->GetBinContent(bi);
      tot->SetBinContent(bi,y1+y2);
    }
  }
  return tot;
}

TH1D *AddInQuad(vector<TH1D*> Histos_) {
  if (Histos_.size()==0) error("Bad input to AddInQuad");
  TH1D *tot = (TH1D*)Histos_[0]->Clone(Str(Histos_[0]->GetName())+"quad_sum");
  for (int i=1;i<(Int_t)Histos_.size();++i) {
    TH1D *h = Histos_[i];
    if (tot->GetNbinsX()!=h->GetNbinsX()) error("Bad binning");
    for (int bi=1;bi<=tot->GetNbinsX();++bi) {
      double y1=tot->GetBinContent(bi), y2=h->GetBinContent(bi);
      tot->SetBinContent(bi,sqrt(y1*y1+y2*y2));
    }
  }
  return tot;
}

TH1D *AddInQuad(TH1D* h1, TH1D* h2) {
  vector<TH1D*> hs; hs.push_back(h1); hs.push_back(h2);
  return AddInQuad(hs);
}

TH1D *Add(TH1D* h1, TH1D* h2) {
  vector<TH1D*> hs; hs.push_back(h1); hs.push_back(h2);
  return Add(hs);
}
TH1D *Subtract(TH1D* h1, TH1D* h2) {
  vector<TH1D*> hs; hs.push_back(h1); hs.push_back(h2); hs.back()->Scale(-1);
  return Add(hs);
}

double CombineError(double e1, double e2) {
  if (e1<=0||e2<=0) error("Cannot combine errors");
  return 1.0/sqrt(1.0/e1/e1+1.0/e2/e2);
}

double Combine(double y1, double e1, double y2, double e2) {
  if (e1<=0||e2<=0) error("Cannot combine");
  double w1=1.0/e1/e1, w2=1.0/e2/e2;
  return (w1*y1+w2*y2)/(w1+w2);
}

double Comb(double y1, double e1, double y2, double e2) {
  if (e1==0||e2==0) return (y1+y2)/2;
  return (y1/e1/e1+y2/e2/e2)/(1.0/e1/e1+1.0/e2/e2);
}

double CombErr(double e1, double e2) {
  if (e1==0||e2==0) return 0;
  return 1.0/sqrt(1.0/e1/e1+1.0/e2/e2);
}

double CombPoint(Graph *g1, Graph *g2, int i) {
  return Comb(g1->GetY()[i],g1->GetEY()[i],-g2->GetY()[i],g2->GetEY()[i]);
}
double CombPointError(Graph *g1, Graph *g2, int i) {
  return CombErr(g1->GetEY()[i],g2->GetEY()[i]);
}

void Mirror(TH1D *h) {
  for (int i=0;i<=h->GetNbinsX()+1;++i) 
    h->SetBinContent(i,-h->GetBinContent(i));
}

void DrawNuisance(vector<TH1D*> nuisVec, int col=kPink) {
  int Nn=nuisVec.size();
  if (Nn==0) error("No nuis");
  nuisVec[0]->SetMinimum(-0.01); nuisVec[0]->SetMaximum(0.04);
  nuisVec[0]->SetYTitle("Uncertainty");
  nuisVec[0]->SetLineColor(col-9); nuisVec[0]->Draw("L");
  for (int ni=1;ni<Nn;++ni) {
    nuisVec[ni]->SetLineColor(col-9+ni);
    nuisVec[ni]->Draw("sameL");
  }
}



#endif // #ifdef Utils_h

