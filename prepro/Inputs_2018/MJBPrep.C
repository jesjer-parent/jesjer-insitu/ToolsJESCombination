
#include <iostream>
#include <vector>
#include <TRegexp.h>
#include <TH1.h>

#include "utils/Utils.h"

using namespace std;

int MJBPrep(TString EM_PF = "EMPFlow", TString channel = "all", TString rebinningVersion = "2sig")
{
    TH1::SetDefaultSumw2();
    TString path = "MJBInputs/"+EM_PF+"/Files_22_10_18";

    //    TString date = "14-09-20";
    //    TFile *input = TFile::Open(path + "/MJB2015_AntiKt4EMTopo_v2.root");
    TFile *input = TFile::Open(path + "/MJB_"+EM_PF+"_results.root");
    TFile *output = TFile::Open(path + "/MJB_JES_"+ EM_PF +"_"+rebinningVersion+"Rebinning_xmlReady.root", "recreate");
    
    //StrV jetAlgos = Vectorize("AntiKt4TopoEM AntiKt4LCTopo AntiKt6TopoEM AntiKt6LCTopo");
    StrV jetAlgos = Vectorize("AntiKt4" + EM_PF);
     
    StrV syst_MJB = Vectorize("Nominal MCType Stat JVT__1down JVT__1up");

    StrV syst_ES = Vectorize("Alpha20__1down Alpha40__1up Asym70__1down Asym90__1up Beta15__1up Beta5__1down Threshold20__1down Threshold30__1up");

    StrV syst_EI = Vectorize("Modelling__1up NonClosure_highE__1up NonClosure_negEta__1up NonClosure_posEta__1up TotalStat__1up");

    StrV syst_F = Vectorize("Composition__1up Response__1up");

    StrV syst_GJ = Vectorize("GamESZee__1up GamEsmear__1up Generator__1up Jvt__1up Veto__1up Purity__1up ShowerTopology__1up dPhi__1up Stat1__1up Stat2__1up Stat3__1up Stat4__1up Stat5__1up Stat6__1up Stat7__1up Stat8__1up Stat9__1up Stat10__1up Stat11__1up Stat12__1up Stat13__1up Stat14__1up Stat15__1up Stat16__1up");

    StrV syst_PU = Vectorize("OffsetNPV__1up OffsetMu__1up PtTerm__1up RhoTopology__1up");

    StrV syst_ZJ = Vectorize("ElecESZee__1up ElecEsmear__1up Jvt__1up MC__1up MuSagittaRes__1up MuSagittaRho__1up MuScale__1up MuSmearID__1up MuSmearMS__1up ShowerTopology__1up Veto__1up dPhi__1up MuStat1__1up MuStat2__1up MuStat3__1up MuStat4__1up MuStat5__1up MuStat6__1up MuStat7__1up MuStat8__1up MuStat9__1up MuStat10__1up MuStat11__1up MuStat12__1up MuStat13__1up MuStat14__1up ElecStat1__1up ElecStat2__1up ElecStat3__1up ElecStat4__1up ElecStat5__1up ElecStat6__1up ElecStat7__1up ElecStat8__1up ElecStat9__1up ElecStat10__1up ElecStat11__1up ElecStat12__1up ElecStat13__1up ElecStat14__1up ");

    StrV syst_PT = Vectorize("MC16__1up");

    
    //    const int nPtArray = 11;
    //    Double_t ptBins[nPtArray+1] = {300, 400, 500, 600, 700, 800, 900, 1100, 1300, 1600, 1900, 2400};

    
    for (int iAlg = 0; iAlg < jetAlgos.size(); ++iAlg) {
        Str algoName = jetAlgos.at(iAlg);

	///MJB
	for (unsigned int iSyst = 0; iSyst < syst_MJB.size(); ++iSyst) {
	  TString systName = syst_MJB.at(iSyst);
	  std::cout << "SYSTNAME " << systName << std::endl;
	  // Get the stat component from the error on nominal
	  TGraphErrors *g_syst;
	  if (systName.Contains("Stat")){
	    g_syst = GetGraph(input, "graph_Nominal"); 	  
	    }
	  else{
	    g_syst = GetGraph(input, "graph_" + systName); 	  
	  }
	  
	  int nBins = g_syst->GetN(); 
	  Double_t ax[nBins],ay[nBins];
	  Double_t binArray[nBins+1];

	  for (int i = 0; i < nBins; ++i){
	    g_syst->GetPoint(i,ax[i], ay[i]);
	    binArray[i]=ax[i]-g_syst->GetErrorX(i);
	  }
	  binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);
	  
	  systName.ReplaceAll("_1up", "up");
          systName.ReplaceAll("_1down", "down");                                                                                                                                                             
          systName.ReplaceAll("MCType", "MC");                                                                                                                                                              
	  TH1D *h_syst = new TH1D("MJB_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
	  for (int i = 1; i <= g_syst->GetN(); ++i) {
	    if (systName.Contains("Stat")){
	      h_syst->SetBinContent(i, g_syst->GetErrorY(i-1));
	    }
	    else {
	      h_syst->SetBinContent(i, ay[i-1]);
	      h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	    }
	  }

	  output->cd();
	  h_syst->Write();
	  delete h_syst;
	  
	}          

    ///EvSel
    for (unsigned int iSyst = 0; iSyst < syst_ES.size(); ++iSyst) {
      TString systName = syst_ES.at(iSyst);
      std::cout << "SYSTNAME " << systName << std::endl;
      TGraphErrors *g_syst = GetGraph(input, "graph_EvSel_" + systName);

      int nBins = g_syst->GetN();
      Double_t ax[nBins],ay[nBins];
      Double_t binArray[nBins+1];

      for (int i = 0; i < nBins; ++i){
	g_syst->GetPoint(i,ax[i], ay[i]);
	binArray[i]=ax[i]-g_syst->GetErrorX(i);
      }
      binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);

      systName.ReplaceAll("_1up", "up");
      systName.ReplaceAll("_1down", "down");

      //Remove all numbers from the systnames
      // Giving up on doing this is a smart way
      //TRegexp RE_numeric = new TRegexp('[0-9]'); 
      //Ssiz_t n1Index = systName.Index(RE_numeric);
      //systName.Remove(n1Index);

      systName.ReplaceAll("Alpha20", "Alpha");
      systName.ReplaceAll("Alpha40", "Alpha");
      systName.ReplaceAll("Asym70", "Asym");
      systName.ReplaceAll("Asym90", "Asym");
      systName.ReplaceAll("Beta15", "Beta");
      systName.ReplaceAll("Beta5", "Beta");
      systName.ReplaceAll("Threshold20", "Threshold");
      systName.ReplaceAll("Threshold30", "Threshold");


      TH1D *h_syst = new TH1D("MJB_ES_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
      for (int i = 1; i <= g_syst->GetN(); ++i) {
	//std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                             
	h_syst->SetBinContent(i, ay[i-1]);
	h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
      }

      h_syst->Write();
      delete h_syst;
    }


    ///Eta Intercalibration 
    for (unsigned int iSyst = 0; iSyst < syst_EI.size(); ++iSyst) {
      TString systName = syst_EI.at(iSyst);
      std::cout << "SYSTNAME " << systName << std::endl;
      TGraphErrors *g_syst = GetGraph(input, "graph_JET_EtaIntercalibration_" + systName);

      int nBins = g_syst->GetN();
      Double_t ax[nBins],ay[nBins];
      Double_t binArray[nBins+1];

      for (int i = 0; i < nBins; ++i){
	g_syst->GetPoint(i,ax[i], ay[i]);
	binArray[i]=ax[i]-g_syst->GetErrorX(i);
      }
      binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);

      systName.ReplaceAll("_1up", "up");

      TH1D *h_syst = new TH1D("MJB_EI_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
      for (int i = 1; i <= g_syst->GetN(); ++i) {
	//std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                             
	h_syst->SetBinContent(i, ay[i-1]);
	h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
      }

      if(systName.Contains("_up")){
	systName.ReplaceAll("_up", "_down");
	TH1D *h_syst_down = new TH1D("MJB_EI_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
	for (int i = 1; i <= g_syst->GetN(); ++i) {
	  //std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                                 
	  h_syst->SetBinContent(i, ay[i-1]);
	  h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	}
	output->cd();
	h_syst_down->Write();
	delete h_syst_down;
      }
      output->cd();
      h_syst->Write();
      delete h_syst;
    }

    ///Flavor 
    for (unsigned int iSyst = 0; iSyst < syst_F.size(); ++iSyst) {
      TString systName = syst_F.at(iSyst);
      std::cout << "SYSTNAME " << systName << std::endl;
      TGraphErrors *g_syst = GetGraph(input, "graph_JET_Flavor_" + systName);

      int nBins = g_syst->GetN();
      Double_t ax[nBins],ay[nBins];
      Double_t binArray[nBins+1];

      for (int i = 0; i < nBins; ++i){
	g_syst->GetPoint(i,ax[i], ay[i]);
	binArray[i]=ax[i]-g_syst->GetErrorX(i);
      }
      binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);

      systName.ReplaceAll("_1up", "up");

      TH1D *h_syst = new TH1D("MJB_F_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
      for (int i = 1; i <= g_syst->GetN(); ++i) {
	//std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                             
	h_syst->SetBinContent(i, ay[i-1]);
	h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
      }

      if(systName.Contains("_up")){
	systName.ReplaceAll("_up", "_down");
	TH1D *h_syst_down = new TH1D("MJB_F_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
	for (int i = 1; i <= g_syst->GetN(); ++i) {
	  //std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                                 
	  h_syst->SetBinContent(i, ay[i-1]);
	  h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	}
	output->cd();
	h_syst_down->Write();
	delete h_syst_down;
      }
      output->cd();
      h_syst->Write();
      delete h_syst;
    }



    ///GJet
    for (unsigned int iSyst = 0; iSyst < syst_GJ.size(); ++iSyst) {
      TString systName = syst_GJ.at(iSyst);
      std::cout << "SYSTNAME " << systName << std::endl;
      TGraphErrors *g_syst = GetGraph(input, "graph_JET_Gjet_" + systName);

      int nBins = g_syst->GetN();
      Double_t ax[nBins],ay[nBins];
      Double_t binArray[nBins+1];

      for (int i = 0; i < nBins; ++i){
	g_syst->GetPoint(i,ax[i], ay[i]);
	binArray[i]=ax[i]-g_syst->GetErrorX(i);
      }
      binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);

      systName.ReplaceAll("_1up", "up");

      TH1D *h_syst = new TH1D("MJB_Gjet_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
      for (int i = 1; i <= g_syst->GetN(); ++i) {
	//std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                             
	h_syst->SetBinContent(i, ay[i-1]);
	h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
      }

      if(systName.Contains("_up")){
	systName.ReplaceAll("_up", "_down");
	TH1D *h_syst_down = new TH1D("MJB_Gjet_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
	for (int i = 1; i <= g_syst->GetN(); ++i) {
	  //std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                                 
	  h_syst->SetBinContent(i, ay[i-1]);
	  h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	}
	output->cd();
	h_syst_down->Write();
	delete h_syst_down;
      }
      output->cd();
      h_syst->Write();
      delete h_syst;
    }


    ///Zjet
    for (unsigned int iSyst = 0; iSyst < syst_ZJ.size(); ++iSyst) {
      TString systName = syst_ZJ.at(iSyst);
      std::cout << "SYSTNAME " << systName << std::endl;
      TGraphErrors *g_syst = GetGraph(input, "graph_JET_Zjet_" + systName);

      int nBins = g_syst->GetN();
      Double_t ax[nBins],ay[nBins];
      Double_t binArray[nBins+1];

      for (int i = 0; i < nBins; ++i){
	g_syst->GetPoint(i,ax[i], ay[i]);
	binArray[i]=ax[i]-g_syst->GetErrorX(i);
      }
      binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);

      systName.ReplaceAll("_1up", "up");

      TH1D *h_syst = new TH1D("MJB_Zjet_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
      for (int i = 1; i <= g_syst->GetN(); ++i) {
	//std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                             
	h_syst->SetBinContent(i, ay[i-1]);
	h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
      }

      if(systName.Contains("_up")){
	systName.ReplaceAll("_up", "_down");
	TH1D *h_syst_down = new TH1D("MJB_Zjet_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
	for (int i = 1; i <= g_syst->GetN(); ++i) {
	  //std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;                                                                                                                                 
	  h_syst->SetBinContent(i, ay[i-1]);
	  h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	}
	output->cd();
	h_syst_down->Write();
	delete h_syst_down;
      }
      output->cd();
      h_syst->Write();
      delete h_syst;
    }

    ///Pile-up
    for (unsigned int iSyst = 0; iSyst < syst_PU.size(); ++iSyst) {
      TString systName = syst_PU.at(iSyst);
      std::cout << "SYSTNAME " << systName << std::endl;
      TGraphErrors *g_syst = GetGraph(input, "graph_JET_Pileup_" + systName);

      int nBins = g_syst->GetN();
      Double_t ax[nBins],ay[nBins];
      Double_t binArray[nBins+1];

      for (int i = 0; i < nBins; ++i){
	g_syst->GetPoint(i,ax[i], ay[i]);
	binArray[i]=ax[i]-g_syst->GetErrorX(i);
      }
      binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);

      systName.ReplaceAll("_1up", "up");

      TH1D *h_syst = new TH1D("MJB_PU_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
      for (int i = 1; i <= g_syst->GetN(); ++i) {
	h_syst->SetBinContent(i, ay[i-1]);
	h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
      }

      if(systName.Contains("_up")){
	systName.ReplaceAll("_up", "_down");
	TH1D *h_syst_down = new TH1D("MJB_PU_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
	for (int i = 1; i <= g_syst->GetN(); ++i) {
	  h_syst->SetBinContent(i, ay[i-1]);
	  h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	}
	output->cd();
	h_syst_down->Write();
	delete h_syst_down;
      }
      output->cd();
      h_syst->Write();
      delete h_syst;
    }


    ///Pile-up
    for (unsigned int iSyst = 0; iSyst < syst_PT.size(); ++iSyst) {
      TString systName = syst_PT.at(iSyst);
      std::cout << "SYSTNAME " << systName << std::endl;
      TGraphErrors *g_syst = GetGraph(input, "graph_JET_PunchThrough_" + systName);

      int nBins = g_syst->GetN();
      Double_t ax[nBins],ay[nBins];
      Double_t binArray[nBins+1];

      for (int i = 0; i < nBins; ++i){
	g_syst->GetPoint(i,ax[i], ay[i]);
	binArray[i]=ax[i]-g_syst->GetErrorX(i);
      }
      binArray[nBins] = ax[nBins-1] + g_syst->GetErrorX(nBins-1);

      systName.ReplaceAll("_1up", "up");

      TH1D *h_syst = new TH1D("MJB_PT_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
      for (int i = 1; i <= g_syst->GetN(); ++i) {
	h_syst->SetBinContent(i, ay[i-1]);
	h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
      }

      if(systName.Contains("_up")){
	systName.ReplaceAll("_up", "_down");
	TH1D *h_syst_down = new TH1D("MJB_PT_" + systName + "_"+algoName, "", g_syst->GetN(), binArray);
	for (int i = 1; i <= g_syst->GetN(); ++i) {
	  h_syst->SetBinContent(i, ay[i-1]);
	  h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	}
	output->cd();
	h_syst_down->Write();
	delete h_syst_down;
      }
      output->cd();
      h_syst->Write();
      delete h_syst;
    }

    }

    output->Close();
    return 0;
}
