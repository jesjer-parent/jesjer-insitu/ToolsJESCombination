#include <iostream>
#include <vector>
#include <TH1.h>
#include "utils/Utils.h"

using namespace std;

int rTrackPrepJMS(TString path = "rTrackInput/Files_18_05_22/", TString channel = "all", TString release = "21")
{
    TH1::SetDefaultSumw2();
    
    TFile *file_gj = TFile::Open(path + "maps_rTrk_eta2p0.root");
    TFile *outputFile = TFile::Open(path + "/rTrack_JMS_LC_rel"+release+"Rebinning_xmlReady.root", "recreate");
    
    Str variable = "m"; // could be pt
    StrV massbin = Vectorize("m1 m2 m3 m4");
    StrV syst = Vectorize("pythia herwig stat pythia_TC1 pythia_TC2 pythia_TC3");    

    //    const int nPtArray = 11;
    // Double_t ptBins[nPtArray+1] = {300, 400, 500, 600, 700, 800, 900, 1100, 1300, 1600, 1900, 2400};
    
    //    int Emin = 300.0;
    // int Emax = 3000.0;

    std::cout << "Number of systematics " << syst.size() << std::endl;

    for (unsigned int iBin = 1; iBin <= massbin.size(); ++iBin) {
      for (unsigned int iSyst = 0; iSyst < syst.size(); ++iSyst) {
	TString systName = syst.at(iSyst);
	Str binName = massbin.at(iBin-1);
	TH2D *g_syst;
	TH2D *nom_hist;

	nom_hist = Get2DHisto(file_gj, "pythia_" +variable);

	if (systName.Contains("stat")){
	  g_syst = Get2DHisto(file_gj, "pythia_" +variable);}
	else{
	  g_syst = Get2DHisto(file_gj, systName +"_" +variable);}

	std::cout << systName << std::endl;
	
	TH1D *h_syst;

	if (systName.Contains("stat")){
	    h_syst = new TH1D("Rtrk_Stat_" +binName, "",g_syst->GetNbinsX(), g_syst->GetXaxis()->GetXbins()->GetArray());
	    for (int i = 1; i <= h_syst->GetNbinsX(); ++i) {
	      h_syst->SetBinContent(i, g_syst->GetBinError(i,iBin)/(1-g_syst->GetBinContent(i,iBin)));
	      //std::cout << g_syst->GetBinContent(i, iBin) << std::endl;
	    } 
	  }
	else if (systName.Contains("TC")) {
	  h_syst = new TH1D("Rtrk_" +systName +"_" +binName, "",g_syst->GetNbinsX(), g_syst->GetXaxis()->GetXbins()->GetArray());
	  for (int i = 1; i <= h_syst->GetNbinsX(); ++i) {
	    h_syst->SetBinContent(i, g_syst->GetBinContent(i,iBin)/(1-nom_hist->GetBinContent(i,iBin)));
	    //std::cout << g_syst->GetBinContent(i, iBin) << std::endl;
	    h_syst->SetBinError(i, g_syst->GetBinError(i,iBin));
	  }
	}
	else if (systName.Contains("herwig")){
	  h_syst = new TH1D("Rtrk_Model_" +binName, "",g_syst->GetNbinsX(), g_syst->GetXaxis()->GetXbins()->GetArray());
	  for (int i = 1; i <= h_syst->GetNbinsX(); ++i) {
	    h_syst->SetBinContent(i, g_syst->GetBinContent(i,iBin)/(1-nom_hist->GetBinContent(i,iBin)));
	    //std::cout << g_syst->GetBinContent(i, iBin) << std::endl;
	    h_syst->SetBinError(i, g_syst->GetBinError(i,iBin));
	  }
	}
	else{
	  h_syst = new TH1D("Rtrk_Nominal_" +binName, "",g_syst->GetNbinsX(), g_syst->GetXaxis()->GetXbins()->GetArray());
	  for (int i = 1; i <= h_syst->GetNbinsX(); ++i) {
	    h_syst->SetBinContent(i, 1-g_syst->GetBinContent(i,iBin));
	    //std::cout << g_syst->GetBinContent(i, iBin) << std::endl;
	    h_syst->SetBinError(i, g_syst->GetBinError(i,iBin));
	  }
	}


      outputFile->cd();
      h_syst->Write();
      delete h_syst;
      }
    }

outputFile->Close();

return 0;
}

/*
    for (unsigned int iAlg = 0; iAlg < jetAlgos.size(); ++iAlg) {
        Str algoName = jetAlgos.at(iAlg);

	///rTrk
	for (unsigned int iSyst = 0; iSyst < syst_Rtrk.size(); ++iSyst) {
	  TString systName = syst_Rtrk.at(iSyst);
	  std::cout << iSyst << " SYSTNAME " << systName << std::endl;
	  TGraphErrors *g_syst = GetGraph(input, "Rtrk_" + systName +"_pT_"+ algoName); 	  
	  vector<double> AX;
	  AX.push_back(Emin);

	  int nBins = g_syst->GetN();
	  std::cout << nBins << std::endl;

	  Double_t ax[nBins],ay[nBins];
	  for (int i = 0; i < g_syst->GetN(); ++i){
	    g_syst->GetPoint(i,ax[i], ay[i]);
	    //std::cout << "ax1 " << AX[i] << std::endl;
	    //std::cout << "X "<< ax[i] << std::endl;
	    //	    std::cout << "Y " << ay[i] << std::endl;
	    if (i < g_syst->GetN()-1){
	      AX.push_back(2*ax[i]-AX[i]);}
	      //std::cout << "ax2 " << AX[i+1] << std::endl;}
	      //std::cout << " " << std::endl;
	  }

	  AX.push_back(Emax);
	  double* xbins = &AX[0];
	  
	  TH1D *h_syst = new TH1D("Rtrk_" + systName + "_"+algoName, "", AX.size()-1,xbins);

	  for (int i = 1; i <= g_syst->GetN(); ++i) {
	    //	    std::cout << ay[i-1] << " " << g_syst->GetErrorY(i-1) << std::endl;
	    h_syst->SetBinContent(i, ay[i-1]);
	    h_syst->SetBinError(i, g_syst->GetErrorY(i-1));
	  }


	  output->cd();
	  h_syst->Write();
	  delete h_syst;
	}    
    }

    output->Close();
    return 0;
}

*/
