#include <iostream>
#include <vector>
#include <TH1.h>
#include "../utils/Utils.h"

using namespace std;

int rTrackPrep_TA(TString path = "rTrackInput/Files_20_02_25/", TString channel = "all", TString release = "21")
{
    TH1::SetDefaultSumw2();

    Str variable = "m"; // could be pt
    StrV massbin = Vectorize("m1 m2 m3");
    Str algoName = "AntiKt10LCTopoTrimmedPtFrac5SmallR20";
    
    TFile *file_rtk = TFile::Open(path + "/rTrack_input.root");
    TFile *file_ff = TFile::Open(path + "/FFttbar_CaloMass_FinnerBinning.root");
    TFile *outputFile = TFile::Open(path + "/rTrack_JMS_rel"+release+"_TAMass_xmlReady.root", "recreate");
    
    StrV syst = Vectorize("pythia modeling stat pythia_TC1 pythia_TC2 pythia_TC3");  //sherpa?   

    std::cout << "Number of systematics " << syst.size() << std::endl;

    for (unsigned int iBin = 1; iBin <= massbin.size(); ++iBin) {
      for (unsigned int iSyst = 0; iSyst < syst.size(); ++iSyst) {
	TString systName = syst.at(iSyst);
	Str binName = massbin.at(iBin-1);
	TH2D *g_syst;
	TH2D *ff_hist_org;
	TH1D *ff_hist; 
	//TH2D *ff_stat_org;
	//TH1D *ff_stat; 

	ff_hist_org = Get2DHisto(file_ff, "plots/FF_Nom_JMS");     
	ff_hist = ff_hist_org->ProjectionY("", 0, -1, "");

	//ff_stat_org = Get2DHisto(file_ff, "plots/FF_Stat_JMS");     
	//ff_stat = ff_stat_org->ProjectionY("", 0, -1, "");


	//if (systName.Contains("pythia")){
	// g_syst = Get2DHisto(file_ff, "plots/FF_Stat_JMS");     
	//  ff_hist_org = Get2DHisto(file_ff, "plots/FF_Nom_JMS");     
	//  ff_hist = ff_hist_org->ProjectionY("", 0, -1, "");	    
	//}
	if (systName.Contains("stat")){
	  g_syst = Get2DHisto(file_rtk, "pythia_" +variable + "TrkOverPtTrk");
	  //g_syst = Get2DHisto(file_ff, "plots/FF_Stat_JMS");     
	  //ff_hist_org = Get2DHisto(file_ff, "plots/FF_Stat_JMS");     
	  //ff_hist = ff_hist_org->ProjectionY("", 0, -1, "");
	}
	else{
	  g_syst = Get2DHisto(file_rtk, systName +"_" +variable + "TrkOverPtTrk");
	}

	std::cout<<"WTF: "<< ff_hist->GetBinContent(10)<<std::endl;

	int nPtBins = g_syst->GetNbinsX();
	int nBinsFF = ff_hist->GetNbinsX();

	const Double_t* binsOrg = g_syst->GetXaxis()->GetXbins()->GetArray();
	vector<double> binVec (binsOrg, binsOrg + 7);

	//binVec.erase(binVec.begin()+6); // Remove last bin

	int nSkip = 0; // Number of bins to rm from beginning og hist

	std::cout << binName << std::endl;
	double* bins = &binVec[0];  

	if (binName.Contains("m3")){
	  nPtBins = nPtBins - 1;
	  nSkip = 1;
	  bins = &binVec[0+nSkip];  
	}
	
	//for (int k=0; k<=nPtBins; k++){
	//  std::cout << bins[k] <<std::endl;
	//}

	const Double_t* binsFF = ff_hist->GetXaxis()->GetXbins()->GetArray();


	std::cout << systName << std::endl;
	
	TH1D *h_syst;

	if (systName.Contains("TC")) {
	  h_syst = new TH1D("Rtrk_" +systName +"_" +algoName + "_"+binName, "",nPtBins, bins);
	  for (int i = 1; i <= nPtBins; ++i) {
	    h_syst->SetBinContent(i, g_syst->GetBinContent(i+nSkip,iBin));
	    h_syst->SetBinError(i, g_syst->GetBinError(i+nSkip,iBin));
	  }
	}
	else if (systName.Contains("modeling")){
	  h_syst = new TH1D("Rtrk_Model_" +algoName + "_"+binName, "",nPtBins, bins);
	  for (int i = 1; i <= nPtBins; ++i) {
	    h_syst->SetBinContent(i, g_syst->GetBinContent(i+nSkip,iBin));
	    h_syst->SetBinError(i, g_syst->GetBinError(i+nSkip,iBin));
	  }
	}

	else if (systName.Contains("stat")){
	  h_syst = new TH1D("Rtrk_Stat_" +algoName+"_"+binName, "",nPtBins, bins);
          for (int i = 1; i <= nPtBins; ++i) {
            h_syst->SetBinContent(i, g_syst->GetBinError(i+nSkip,iBin));
          }

	}

	else{
	  h_syst = new TH1D("Rtrk_Nominal_" +algoName + "_"+binName, "",nPtBins, bins);

	  for (int i = 1; i <= nPtBins; ++i) {

	    int jmin = 0;
	    int jmin2 = 0;
	    double dmin = 10000000;
	    double dmin2 = 10000000;
	    double dij;
	    double scale;
	    double rBinCen;
	    double fBinCen;

	    rBinCen = (bins[i]+bins[i-1])/2;

	    for(int j=1; j<=nBinsFF; j++){ 
	      fBinCen = (binsFF[j]+binsFF[j-1])/2.;
	      dij = rBinCen - fBinCen;

	      if (abs(dij) <= dmin){
		dmin = dij;
		jmin = j;
	      }
	      else if (abs(dij)<=dmin2){
		dmin2 = dij;
		jmin2 = j;
	      }
	    }

	    if (rBinCen>=fBinCen){

	      h_syst->SetBinContent(i, ff_hist->GetBinContent(jmin));
	      h_syst->SetBinError(i, g_syst->GetBinError(i+nSkip,iBin));
	    }
	    
	    else{
	      scale = 1./(1./dmin + 1./dmin2);
	      std::cout << "ff average "<< jmin << ", " << jmin2 << ": " << scale*((1/dmin)*ff_hist->GetBinContent(jmin)+(1/dmin2)*ff_hist->GetBinContent(jmin2)) << std::endl;
	      h_syst->SetBinContent(i, scale*((1/dmin)*ff_hist->GetBinContent(jmin)+(1/dmin2)*ff_hist->GetBinContent(jmin2)) );
	    }
	  }
	}

      outputFile->cd();
      h_syst->Write();
      delete h_syst;
      }
    }

outputFile->Close();

return 0;
}



/*


	  for (int i = 1; i <= nPtBins; ++i) {

	      int jmin = 0;
	      int jmin2 = 0;

	      double dmin = 10000000;
	      double dmin2 = 10000000;
	      double dij;
	      double rBinCen;
	      double fBinCen;
	      double scale;

	      rBinCen = (bins[i]+bins[i-1])/2;
	      std::cout << "rTrack bin center "<< rBinCen << std::endl;

	      for(int j=1; j<=nBinsFF; j++){
		fBinCen = (binsFF[j]+binsFF[j-1])/2.;
		dij = rBinCen - fBinCen; 
		std::cout << "ff bin center"<< fBinCen << std::endl;

		if (abs(dij) <= dmin){
		  dmin = dij;
		  jmin = j;
		}
		else if (abs(dij)<=dmin2){
		  dmin2 = dij;
		  jmin2 = j;
		}
	      }
	      //std::cout << "ff content "<< jmin << ": " << ff_hist->GetBinContent(jmin) << std::endl;

	      if (rBinCen>=fBinCen){
		h_syst->SetBinContent(i, ff_hist->GetBinContent(jmin));
	      }
	      else{
		scale = 1./(1./dmin + 1./dmin2);
		std::cout << "ff average "<< jmin << ", " << jmin2 << ": " << scale*((1/dmin)*ff_hist->GetBinContent(jmin)+(1/dmin2)*ff_hist->GetBinContent(jmin2)) << std::endl;
		h_syst->SetBinContent(i, scale*((1/dmin)*ff_hist->GetBinContent(jmin)+(1/dmin2)*ff_hist->GetBinContent(jmin2)) );
	      }
	    } 
*/
