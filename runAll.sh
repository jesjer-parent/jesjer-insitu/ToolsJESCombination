


mv prepro

# USED /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/gamma_jet_julio.root
# USED /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/gamma_jet_ovidiu.root
# USED /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/mjb_input.root
# USED /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/zjet_ee_input.root
# USED /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/zjet_mumu_input.root
# USED  /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/FFttbar_CaloMass_FinnerBinning.root
# CORRUPTED /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/FFttbar_TAMass_FinnerBinning_nov19.root   # CORRUPTED
# USED  /afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/rTrack_input.root


# hardcoded file names
# hardcoded output path which is same as input path
# many hardcoded input systematics
root -b -q Inputs_2019_LR/GammaJetPrep.C++(\"/afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/\",\"No\") 

# hardcoded input file name
# hardcoded output path to be the same as input path
# many hardcoded input systematics
# had to add hack because of input foermat of where the stat histogram resides
root -b -q Inputs_2019_LR/MJBPrep.C++(\"/afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/\",\"No\") 

# hardcoded input file name
# hardcoded output path to be the same as input path
# many hardcoded input systematics
root -b -q Inputs_2019_LR/ZJetPrep.C++(\"/afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/\",\"ee\",\"zjet_ee_input.root\");
 

root -b -q Inputs_2019_LR/ZJetPrep.C++(\"/afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/\",\"mumu\",\"zjet_mumu_input.root\");

# requires both forward folding and rTrack inputs
root -b -q Inputs_2019_LR/rTrackPrep_TA.C++(\"/afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/\")

# fully hardcoded inputs
root -b -q Inputs_2019_LR/RootToXmlJES.C++\(\"/afs/cern.ch/work/m/meehan/public/InSitu/JESComboInputs/\"\)



# move to exe
cd ../exe


# testappLR needs to be run twice
# with two configs

./testappLR testappLR_stat.xml 

./testappLR testappLR_full.xml 

cd ../data/ComplexMergerResults_JES/Results_LR/
mkdir -p Results_LCJES_R10_ZJee_ZJmm_GJ_MJB_test/Plots
mkdir -p Results_LCJES_R10_ZJee_ZJmm_GJ_MJB_test/SmoothTxtFiles
mv ../*txt Results_LCJES_R10_ZJee_ZJmm_GJ_MJB_test/.


#testappLR puts a lot of text files in 
#data/ComplexMergerResults_JES


cd /afs/cern.ch/work/m/meehan/public/InSitu/ToolsJESCombination/data/ComplexMergerResults_JES/Results_LR


# hardcoded and finnicky NP names
# move to config file or something
# add parsing stage to check for existence of systematics?
# careful of comma at end of NP list
root -b -q JES_JMS_2019/PlotNuisanceParameters.C++(false)
