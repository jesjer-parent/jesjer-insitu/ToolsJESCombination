#! /bin/csh

ln -f -s src include

# check Root environment setup
if !($?ROOTSYS) then
  echo "Warning: Root environment (ROOTSYS) not yet defined. Please do so! "
endif

# The ROOTSYS/lib may be set in a LD_LIBRARY_PATH or using ld.so
grep -q `echo $ROOTSYS/lib /etc/ld.so.cache`
set root_in_ld = $status

if ($?LD_LIBRARY_PATH) then
    setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:$PWD/lib
else 
    setenv LD_LIBRARY_PATH $PWD/lib
    if ( ${root_in_ld} == 1 ) then
        echo "Warning: so far you haven't setup your ROOT enviroment properly (no LD_LIBRARY_PATH or entry in ld.so.cache): ToolsJEScombination, derived from HVPTools, will not work"
    endif
endif

