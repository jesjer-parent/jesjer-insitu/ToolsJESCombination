# analysis base version from which we start
FROM gitlab-registry.cern.ch/jesjer-parent/jesjer-insitu/docker-root:master-c9e5fd51

# put the code into the repo with the proper owner
COPY --chown=atlas . /code/src

# note that the build directory already exists in /code/src
RUN chown -R atlas /code/src && \
    cd /code/src && \
    source setup.sh && \
    cd /code/src/src && \
    make && \
    cd /code/src/exe && \
    make 

# where you are left after booting the image
WORKDIR /code/src


